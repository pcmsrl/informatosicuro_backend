﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RiunionePraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public RiunionePraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/RiunionePratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaRiunionePratica>>> GetRiunionePratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaRiunionePratica
                .Join(listaPraticaAmmessa, listaRiunionePratica => listaRiunionePratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaRiunionePratica, listaPraticaAmmessa) => listaRiunionePratica)
                .OrderByDescending(x => x.IdPratica)
                .ThenBy(x => x.DataRiunione).ToListAsync();
        }

        // GET: api/RiunionePratica/Pratica/5
        [HttpGet]
        [Route("Pratica/{idPratica}")]
        public async Task<ActionResult<IEnumerable<ListaRiunionePratica>>> GetRiunionePratica_Pratica(long idPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaRiunionePratica
                .Join(listaPraticaAmmessa, listaRiunionePratica => listaRiunionePratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaRiunionePratica, listaPraticaAmmessa) => listaRiunionePratica)
                .Where(x => x.IdPratica == idPratica)
                .OrderBy(x => x.DataRiunione).ToListAsync();
        }

        // GET: api/RiunionePratica/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ListaRiunionePratica>> GetRiunionePratica(long id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            var riunionePratica = await _context.ListaRiunionePratica
                .Join(listaPraticaAmmessa, listaRiunionePratica => listaRiunionePratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaRiunionePratica, listaPraticaAmmessa) => listaRiunionePratica)
                .Where(x => x.Id == id).FirstOrDefaultAsync();

            if (riunionePratica == null)
            {
                return NotFound();
            }

            return riunionePratica;
        }

        // PUT: api/RiunionePratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{idRiunionePratica}")]
        public async Task<IActionResult> PutRiunionePratica(long idRiunionePratica, RiunionePratica riunionePratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == riunionePratica.IdPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            if (idRiunionePratica != riunionePratica.Id)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await riunionePratica.Update(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return AcceptedAtAction("PutRiunionePratica", new { idRiunionePratica = riunionePratica.Id }, await _context.ListaRiunionePratica.Where(x => x.Id == idRiunionePratica).FirstOrDefaultAsync());
        }

        // POST: api/RiunionePratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<RiunionePratica>> PostRiunionePratica(RiunionePratica riunionePratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == riunionePratica.IdPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            int result;
            try
            {
                result = await riunionePratica.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostRiunionePratica", new { idRiunione = riunionePratica.Id }, await _context.ListaRiunionePratica.Where(x => x.Id == riunionePratica.Id).FirstOrDefaultAsync());
        }

        // DELETE: api/RiunionePratica/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RiunionePratica>> DeleteRiunionePratica(long id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();


            var riunionePratica = await _context.RiunionePratica.FindAsync(id);
            if (riunionePratica == null)
            {
                return NotFound();
            }

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == riunionePratica.IdPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            int result;

            result = await riunionePratica.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool RiunionePraticaExists(long id)
        {
            return _context.RiunionePratica.Any(e => e.Id == id);
        }
    }
}
