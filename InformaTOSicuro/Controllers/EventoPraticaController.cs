﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventoPraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public EventoPraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/EventoPratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaEventoPratica>>> GetEventoPratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaEventoPratica
                .Join(listaPraticaAmmessa, listaEventoPratica => listaEventoPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaEventoPratica, listaPraticaAmmessa) => listaEventoPratica)
                .OrderByDescending(x => x.IdPratica)
                .ThenByDescending(x => x.DataEvento).ToListAsync();
        }

        // GET: api/EventoPratica/Pratica/5
        [HttpGet("Pratica/{idPratica}")]
        public async Task<ActionResult<IEnumerable<ListaEventoPratica>>> GetEventoPratica(long idPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            var eventoPratica = await _context.ListaEventoPratica.Where(x => x.IdPratica == idPratica)
                .Join(listaPraticaAmmessa, listaEventoPratica => listaEventoPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaEventoPratica, listaPraticaAmmessa) => listaEventoPratica)
                .OrderByDescending(x => x.DataEvento).ToListAsync();

            if (eventoPratica == null)
            {
                return NotFound();
            }

            return eventoPratica;
        }

        // GET: api/EventoPratica/5/1
        [HttpGet("{idPratica}/{numEvento}")]
        public async Task<ActionResult<ListaEventoPratica>> GetEventoPratica(long idPratica, int numEvento)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            var eventoPratica = await _context.ListaEventoPratica
                .Join(listaPraticaAmmessa, listaEventoPratica => listaEventoPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaEventoPratica, listaPraticaAmmessa) => listaEventoPratica)
                .Where(x => x.IdPratica == idPratica && x.NumEvento == numEvento).FirstOrDefaultAsync();

            if (eventoPratica == null)
            {
                return NotFound();
            }

            return eventoPratica;
        }
        
        // PUT: api/EventoPratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{idPratica}/{numEvento}")]
        public async Task<IActionResult> PutEventoPratica(long idPratica, short numEvento, EventoPratica EventoPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == EventoPratica.IdPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            if (idPratica != EventoPratica.IdPratica || numEvento != EventoPratica.NumEvento)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await EventoPratica.Update(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return AcceptedAtAction("PutEventoPratica", new { idPratica = EventoPratica.IdPratica, numEvento = EventoPratica.NumEvento }, await _context.ListaEventoPratica.Where(x => x.IdPratica == EventoPratica.IdPratica && x.NumEvento == EventoPratica.NumEvento).FirstOrDefaultAsync());
        }

        // POST: api/EventoPratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<EventoPratica>> PostEventoPratica(EventoPratica EventoPratica)
        {
            int result;
            try
            {
                if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
                Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

                ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == EventoPratica.IdPratica).FirstOrDefaultAsync();

                if (praticaAmmessa == null) return Unauthorized();

                result = await EventoPratica.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostEventoPratica", new { idPratica = EventoPratica.IdPratica, numEvento = EventoPratica.NumEvento }, await _context.ListaEventoPratica.Where(x => x.IdPratica == EventoPratica.IdPratica && x.NumEvento == EventoPratica.NumEvento).FirstOrDefaultAsync());
        }

        // DELETE: api/EventoPratica/1/1
        [HttpDelete("{idPratica}/{numEvento}")]
        public async Task<ActionResult<EventoPratica>> DeleteEventoPratica(long idPratica, short numEvento)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == idPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            var EventoPratica = await _context.EventoPratica.Where(x => x.IdPratica == idPratica && x.NumEvento == numEvento).FirstOrDefaultAsync();
            if (EventoPratica == null)
            {
                return NotFound();
            }

            int result;

            result = await EventoPratica.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool EventoPraticaExists(long id)
        {
            return _context.EventoPratica.Any(e => e.IdPratica == id);
        }
    }
}
