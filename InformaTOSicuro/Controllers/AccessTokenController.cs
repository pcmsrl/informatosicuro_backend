﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;
using System.Net.Http.Headers;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccessTokenController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public AccessTokenController(InformaTOSicuroContext context)
        {
            _context = context;
        }
        /*
        // GET: api/AccessToken
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AccessToken>>> GetAccessToken()
        {
            return await _context.AccessToken.ToListAsync();
        }*/
        /*
        // GET: api/AccessToken/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AccessToken>> GetAccessToken(long id)
        {
            var accessToken = await _context.AccessToken.FindAsync(id);

            if (accessToken == null)
            {
                return NotFound();
            }

            return accessToken;
        }
        */
        /*
        // PUT: api/AccessToken/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccessToken(long id, AccessToken accessToken)
        {
            if (id != accessToken.Id)
            {
                return BadRequest();
            }

            _context.Entry(accessToken).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccessTokenExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/AccessToken
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<AccessToken>> PostAccessToken()
        {

            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            AccessToken accessToken = new AccessToken()
            {
                DataCreazione = DateTime.Now
            };

            int result;

            ListaUtente listaUtente = await _context.ListaUtente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            result = await accessToken.Insert(_context, listaUtente);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return accessToken;
        }

        // DELETE: api/AccessToken/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AccessToken>> DeleteAccessToken(long id)
        {

            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var accessToken = await _context.AccessToken.FindAsync(id);
            if (accessToken == null)
            {
                return NotFound();
            }

            _context.AccessToken.Remove(accessToken);
            await _context.SaveChangesAsync();

            return accessToken;
        }

        private bool AccessTokenExists(long id)
        {
            return _context.AccessToken.Any(e => e.Id == id);
        }
    }
}
