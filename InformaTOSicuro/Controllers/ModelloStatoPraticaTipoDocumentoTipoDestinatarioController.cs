﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;
using InformaTOSicuro.ModelExtension;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModelloStatoPraticaTipoDocumentoTipoDestinatarioController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public ModelloStatoPraticaTipoDocumentoTipoDestinatarioController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/ModelloStatoPraticaTipoDocumentoTipoDestinatario
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaModelloStatoPraticaTipoDocumentoTipoDestinatario>>> GetModelloStatoPraticaTipoDocumentoTipoDestinatario()
        {
            return await _context.ListaModelloStatoPraticaTipoDocumentoTipoDestinatario
                .OrderBy(x => x.ModelloPratica)
                .ThenBy(x => x.TipoDocumento)
                .ThenBy(x => x.TipoDestinatario).ToListAsync();
        }

        // GET: api/ModelloStatoPraticaTipoDocumentoTipoDestinatario/ModelloPratica/1
        [HttpGet]
        [Route("ModelloPratica/{idModello}")]
        public async Task<ActionResult<IEnumerable<ListaModelloStatoPraticaTipoDocumentoTipoDestinatario>>> GetModelloStatoPraticaTipoDocumentoTipoDestinatario_ModelloPratica(int idModello, [FromQuery] Filtro_ModelloStatoPraticaTipoDocumentoTipoDestinatario filtro)
        {
            if (filtro == null)
            {
                return await _context.ListaModelloStatoPraticaTipoDocumentoTipoDestinatario
                    .Where(x => x.IdModelloPratica == idModello)
                    .OrderBy(x => x.TipoDocumento)
                    .ThenBy(x => x.TipoDestinatario).ToListAsync();
            }
            else
            {
                return await _context.ListaModelloStatoPraticaTipoDocumentoTipoDestinatario
                        .Where(x => x.IdModelloPratica == idModello
                            && (string.IsNullOrEmpty(filtro.CodStatoPratica) || x.CodStatoPratica == filtro.CodStatoPratica)
                            && (string.IsNullOrEmpty(filtro.CodTipoDestinatario) || x.CodTipoDestinatario == filtro.CodTipoDestinatario))
                        .OrderBy(x => x.TipoDocumento)
                        .ThenBy(x => x.TipoDestinatario)
                        .ToListAsync();
            }
            
        }

        // GET: api/ModelloStatoPraticaTipoDocumentoTipoDestinatario/5
        [HttpGet("{idModello}/{codStato}/{codTipoDocumento}/{codTipoDestinatario}")]
        public async Task<ActionResult<ListaModelloStatoPraticaTipoDocumentoTipoDestinatario>> GetModelloStatoPraticaTipoDocumentoTipoDestinatario(int idModello, string codStatoPratica, string codTipoDocumento, string CodTipoDestinatario)
        {
            var modelloStatoPraticaTipoDocumentoTipoDestinatario = await _context.ListaModelloStatoPraticaTipoDocumentoTipoDestinatario.Where(x => x.IdModelloPratica == idModello && x.CodStatoPratica == codStatoPratica && x.CodTipoDocumento == codTipoDocumento && x.CodTipoDestinatario == CodTipoDestinatario).FirstOrDefaultAsync();

            if (modelloStatoPraticaTipoDocumentoTipoDestinatario == null)
            {
                return NotFound();
            }

            return modelloStatoPraticaTipoDocumentoTipoDestinatario;
        }
        /*
        // PUT: api/ModelloStatoPraticaTipoDocumentoTipoDestinatario/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutModelloStatoPraticaTipoDocumentoTipoDestinatario(int id, ModelloStatoPraticaTipoDocumentoTipoDestinatario modelloStatoPraticaTipoDocumentoTipoDestinatario)
        {
            if (id != modelloStatoPraticaTipoDocumentoTipoDestinatario.IdModelloPratica)
            {
                return BadRequest();
            }

            _context.Entry(modelloStatoPraticaTipoDocumentoTipoDestinatario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModelloStatoPraticaTipoDocumentoTipoDestinatarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/ModelloStatoPraticaTipoDocumentoTipoDestinatario
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ModelloStatoPraticaTipoDocumentoTipoDestinatario>> PostModelloStatoPraticaTipoDocumentoTipoDestinatario(ModelloStatoPraticaTipoDocumentoTipoDestinatario modelloStatoPraticaTipoDocumentoTipoDestinatario)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo != "Sgrt" && utente.CodRuolo != "ITAd") return Unauthorized();

            int result;
            try
            {
                result = await modelloStatoPraticaTipoDocumentoTipoDestinatario.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostModelloStatoPraticaTipoDocumentoTipoDestinatario", new { idModelloPratica = modelloStatoPraticaTipoDocumentoTipoDestinatario.IdModelloPratica, codStatoPratica = modelloStatoPraticaTipoDocumentoTipoDestinatario.CodStatoPratica, codTipoDocumento = modelloStatoPraticaTipoDocumentoTipoDestinatario.CodTipoDocumento, codTipodestinatario = modelloStatoPraticaTipoDocumentoTipoDestinatario.CodTipoDestinatario }, await _context.ListaModelloStatoPraticaTipoDocumentoTipoDestinatario.Where(x => x.IdModelloPratica == modelloStatoPraticaTipoDocumentoTipoDestinatario.IdModelloPratica && x.CodStatoPratica == modelloStatoPraticaTipoDocumentoTipoDestinatario.CodStatoPratica && x.CodTipoDocumento == modelloStatoPraticaTipoDocumentoTipoDestinatario.CodTipoDocumento && x.CodTipoDestinatario == modelloStatoPraticaTipoDocumentoTipoDestinatario.CodTipoDestinatario).FirstOrDefaultAsync());
        }

        // DELETE: api/ModelloStatoPraticaTipoDocumentoTipoDestinatario/5
        [HttpDelete("{idModelloPratica}/{codStatoPratica}/{codTipoDocumento}/{codTipoDestinatario}")]
        public async Task<ActionResult<ModelloStatoPraticaTipoDocumentoTipoDestinatario>> DeleteModelloStatoPraticaTipoDocumentoTipoDestinatario(int idModelloPratica, string codStatoPratica, string codTipoDocumento, string codTipoDestinatario)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo != "Sgrt" && utente.CodRuolo != "ITAd") return Unauthorized();

            var modelloStatoPraticaTipoDocumentoTipoDestinatario = await _context.ModelloStatoPraticaTipoDocumentoTipoDestinatario.Where(x => x.IdModelloPratica == idModelloPratica && x.CodStatoPratica == codStatoPratica && x.CodTipoDocumento == codTipoDocumento && x.CodTipoDestinatario == codTipoDestinatario).FirstOrDefaultAsync();
            if (modelloStatoPraticaTipoDocumentoTipoDestinatario == null)
            {
                return NotFound();
            }

            int result;

            result = await modelloStatoPraticaTipoDocumentoTipoDestinatario.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool ModelloStatoPraticaTipoDocumentoTipoDestinatarioExists(int id)
        {
            return _context.ModelloStatoPraticaTipoDocumentoTipoDestinatario.Any(e => e.IdModelloPratica == id);
        }
    }
}
