﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessaggioPraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public MessaggioPraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/MessaggioPratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaMessaggioPratica>>> GetMessaggioPratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaMessaggioPratica
                .Join(listaPraticaAmmessa, listaMessaggioPratica => listaMessaggioPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaMessaggioPratica, listaPraticaAmmessa) => listaMessaggioPratica)
                .OrderByDescending(x => x.IdPratica)
                .ThenByDescending(x => x.DataInserimento).ToListAsync();
        }

        // GET: api/MessaggioPratica/Pratica/1
        [HttpGet]
        [Route("Pratica/{idPratica}")]
        public async Task<ActionResult<IEnumerable<ListaMessaggioPratica>>> GetMessaggioPratica_Pratica(long idPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaMessaggioPratica
                .Join(listaPraticaAmmessa, listaMessaggioPratica => listaMessaggioPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaMessaggioPratica, listaPraticaAmmessa) => listaMessaggioPratica)
                .Where(x => x.IdPratica == idPratica)
                .OrderByDescending(x => x.DataInserimento).ToListAsync();
        }

        // GET: api/MessaggioPratica/5
        [HttpGet("{Id}")]
        public async Task<ActionResult<ListaMessaggioPratica>> GetMessaggioPratica(long Id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            var messaggioPratica = await _context.ListaMessaggioPratica
                .Join(listaPraticaAmmessa, listaMessaggioPratica => listaMessaggioPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaMessaggioPratica, listaPraticaAmmessa) => listaMessaggioPratica)
                .Where(x => x.Id == Id).FirstOrDefaultAsync();

            if (messaggioPratica == null)
            {
                return NotFound();
            }

            return messaggioPratica;
        }

        // PUT: api/MessaggioPratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMessaggioPratica(long id, MessaggioPratica messaggioPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == messaggioPratica.IdPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            if (id != messaggioPratica.Id)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await messaggioPratica.Update(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return AcceptedAtAction("PutMessaggioPratica", new { idMessaggio = messaggioPratica.Id }, await _context.ListaMessaggioPratica.Where(x => x.Id == messaggioPratica.Id).FirstOrDefaultAsync());
        }

        // POST: api/MessaggioPratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MessaggioPratica>> PostMessaggioPratica(MessaggioPratica messaggioPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == messaggioPratica.IdPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            int result;
            try
            {
                result = await messaggioPratica.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostMessaggioPratica", new { idMessaggio = messaggioPratica.Id }, await _context.ListaMessaggioPratica.Where(x => x.Id == messaggioPratica.Id).FirstOrDefaultAsync());
        }

        // DELETE: api/MessaggioPratica/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MessaggioPratica>> DeleteContattoPratica(long id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var messaggioPratica = await _context.MessaggioPratica.FindAsync(id);
            if (messaggioPratica == null)
            {
                return NotFound();
            }

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == messaggioPratica.IdPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            int result;

            result = await messaggioPratica.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool MessaggioPraticaExists(long id)
        {
            return _context.MessaggioPratica.Any(e => e.Id == id);
        }
    }
}
