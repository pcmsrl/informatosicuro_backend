﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatoUtenteController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public StatoUtenteController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/StatoUtente
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StatoUtente>>> GetStatoUtente()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            return await _context.StatoUtente
                .OrderBy(x => x.Descrizione).ToListAsync();
        }

        // GET: api/StatoUtente/5
        [HttpGet("{id}")]
        public async Task<ActionResult<StatoUtente>> GetStatoUtente(string id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var statoUtente = await _context.StatoUtente.FindAsync(id);

            if (statoUtente == null)
            {
                return NotFound();
            }

            return statoUtente;
        }

        // PUT: api/StatoUtente/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStatoUtente(string id, StatoUtente statoUtente)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            if (id != statoUtente.Cod)
            {
                return BadRequest();
            }

            _context.Entry(statoUtente).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StatoUtenteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/StatoUtente
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<StatoUtente>> PostStatoUtente(StatoUtente statoUtente)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            _context.StatoUtente.Add(statoUtente);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (StatoUtenteExists(statoUtente.Cod))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetStatoUtente", new { id = statoUtente.Cod }, statoUtente);
        }

        // DELETE: api/StatoUtente/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<StatoUtente>> DeleteStatoUtente(string id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var statoUtente = await _context.StatoUtente.FindAsync(id);
            if (statoUtente == null)
            {
                return NotFound();
            }

            _context.StatoUtente.Remove(statoUtente);
            await _context.SaveChangesAsync();

            return statoUtente;
        }

        private bool StatoUtenteExists(string id)
        {
            return _context.StatoUtente.Any(e => e.Cod == id);
        }
    }
}
