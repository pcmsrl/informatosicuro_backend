﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtenteSpecializzazioneProfessionaleController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public UtenteSpecializzazioneProfessionaleController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/UtenteSpecializzazioneProfessionale
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaUtenteSpecializzazioneProfessionale>>> GetUtenteSpecializzazioneProfessionale()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            return await _context.ListaUtenteSpecializzazioneProfessionale
                .OrderBy(x => x.SpecializzazioneProfessionale)
                .ThenBy(x => x.CognomeUtente)
                .ThenBy(x => x.NomeUtente).ToListAsync();
        }

        // GET: api/UtenteSpecializzazioneProfessionale/SpecializzazioneProfessionale/Nt
        [HttpGet]
        [Route("SpecializzazioneProfessionale/{codSpecializzazioneProfessionale}")]
        public async Task<ActionResult<IEnumerable<ListaUtenteSpecializzazioneProfessionale>>> GetUtenteSpecializzazioneProfessionale_SpecializzazioneProfessionale(string codSpecializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            return await _context.ListaUtenteSpecializzazioneProfessionale
                .Where(x => x.CodSpecializzazioneProfessionale == codSpecializzazioneProfessionale)
                .OrderBy(x => x.CognomeUtente)
                .ThenBy(x => x.NomeUtente).ToListAsync();
        }

        // GET: api/UtenteSpecializzazioneProfessionale/5
        [HttpGet("{username}")]
        public async Task<ActionResult<List<ListaUtenteSpecializzazioneProfessionale>>> GetUtenteSpecializzazioneProfessionale(string username)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            return await _context.ListaUtenteSpecializzazioneProfessionale.Where(x => x.Username == username)
                .OrderBy(x => x.SpecializzazioneProfessionale)
                .ToListAsync();

        }

        // GET: api/UtenteSpecializzazioneProfessionale/5
        [HttpGet("{username}/{codSpecializzazioneProfessionale}")]
        public async Task<ActionResult<ListaUtenteSpecializzazioneProfessionale>> GetUtenteSpecializzazioneProfessionale(string username, string codSpecializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var utenteSpecializzazioneProfessionale = await _context.ListaUtenteSpecializzazioneProfessionale.Where(x => x.Username == username && x.CodSpecializzazioneProfessionale == codSpecializzazioneProfessionale).FirstOrDefaultAsync();

            if (utenteSpecializzazioneProfessionale == null)
            {
                return NotFound();
            }

            return utenteSpecializzazioneProfessionale;
        }
        /*
        // PUT: api/UtenteSpecializzazioneProfessionale/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUtenteSpecializzazioneProfessionale(string id, UtenteSpecializzazioneProfessionale utenteSpecializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            if (id != utenteSpecializzazioneProfessionale.Username)
            {
                return BadRequest();
            }

            _context.Entry(utenteSpecializzazioneProfessionale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UtenteSpecializzazioneProfessionaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/UtenteSpecializzazioneProfessionale
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<UtenteSpecializzazioneProfessionale>> PostUtenteSpecializzazioneProfessionale(UtenteSpecializzazioneProfessionale utenteSpecializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            int result;
            try
            {
                result = await utenteSpecializzazioneProfessionale.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("GetUtenteSpecializzazioneProfessionale", new { Username = utenteSpecializzazioneProfessionale.Username, CodSpecializzazioneProfessionale = utenteSpecializzazioneProfessionale.CodSpecializzazioneProfessionale }, utenteSpecializzazioneProfessionale);
        }

        // DELETE: api/UtenteSpecializzazioneProfessionale/5
        [HttpDelete("{username}/{codSpecializzazioneProfessionale}")]
        public async Task<ActionResult<UtenteSpecializzazioneProfessionale>> DeleteUtenteSpecializzazioneProfessionale(string username, string codSpecializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var utenteSpecializzazioneProfessionale = await _context.UtenteSpecializzazioneProfessionale
                .Where(x => x.Username == username
                    && x.CodSpecializzazioneProfessionale == codSpecializzazioneProfessionale).FirstOrDefaultAsync();

            if (utenteSpecializzazioneProfessionale == null)
            {
                return NotFound();
            }

            int result = await utenteSpecializzazioneProfessionale.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();
        }

        private bool UtenteSpecializzazioneProfessionaleExists(string id)
        {
            return _context.UtenteSpecializzazioneProfessionale.Any(e => e.Username == id);
        }
    }
}
