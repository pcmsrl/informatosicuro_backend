﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoDestinatarioController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public TipoDestinatarioController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/TipoDestinatario
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoDestinatario>>> GetTipoDestinatario()
        {
            return await _context.TipoDestinatario.ToListAsync();
        }

        // GET: api/TipoDestinatario/5
        [HttpGet("{cod}")]
        public async Task<ActionResult<TipoDestinatario>> GetTipoDestinatario(string cod)
        {
            var tipoDestinatario = await _context.TipoDestinatario.FindAsync(cod);

            if (tipoDestinatario == null)
            {
                return NotFound();
            }

            return tipoDestinatario;
        }
        /*
        // PUT: api/TipoDestinatario/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTipoDestinatario(string id, TipoDestinatario tipoDestinatario)
        {
            if (id != tipoDestinatario.Cod)
            {
                return BadRequest();
            }

            _context.Entry(tipoDestinatario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoDestinatarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/
        /*
        // POST: api/TipoDestinatario
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TipoDestinatario>> PostTipoDestinatario(TipoDestinatario tipoDestinatario)
        {
            _context.TipoDestinatario.Add(tipoDestinatario);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TipoDestinatarioExists(tipoDestinatario.Cod))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTipoDestinatario", new { id = tipoDestinatario.Cod }, tipoDestinatario);
        }*/
        /*
        // DELETE: api/TipoDestinatario/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TipoDestinatario>> DeleteTipoDestinatario(string id)
        {
            var tipoDestinatario = await _context.TipoDestinatario.FindAsync(id);
            if (tipoDestinatario == null)
            {
                return NotFound();
            }

            _context.TipoDestinatario.Remove(tipoDestinatario);
            await _context.SaveChangesAsync();

            return tipoDestinatario;
        }*/

        private bool TipoDestinatarioExists(string id)
        {
            return _context.TipoDestinatario.Any(e => e.Cod == id);
        }
    }
}
