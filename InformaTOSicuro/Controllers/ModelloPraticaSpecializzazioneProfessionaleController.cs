﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModelloPraticaSpecializzazioneProfessionaleController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public ModelloPraticaSpecializzazioneProfessionaleController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/ModelloPraticaSpecializzazioneProfessionale
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaModelloPraticaSpecializzazioneProfessionale>>> GetModelloPraticaSpecializzazioneProfessionale()
        {
            return await _context.ListaModelloPraticaSpecializzazioneProfessionale
                .OrderBy(x => x.ModelloPratica)
                .ThenBy(x => x.SpecializzazioneProfessionale).ToListAsync();
        }

        // GET: api/ModelloPraticaSpecializzazioneProfessionale/ModelloPratica/1
        [HttpGet]
        [Route("ModelloPratica/{id}")]
        public async Task<ActionResult<IEnumerable<ListaModelloPraticaSpecializzazioneProfessionale>>> GetModelloPraticaSpecializzazioneProfessionale_ModelloPratica(int id)
        {
            return await _context.ListaModelloPraticaSpecializzazioneProfessionale
                .Where(x => x.IdModelloPratica == id)
                .OrderBy(x => x.SpecializzazioneProfessionale).ToListAsync();
        }

        // GET: api/ModelloPraticaSpecializzazioneProfessionale/5
        [HttpGet("{IdModelloPratica}/{CodSpecializzazioneProfessionale}")]
        public async Task<ActionResult<ListaModelloPraticaSpecializzazioneProfessionale>> GetModelloPraticaSpecializzazioneProfessionale(int IdModelloPratica, string CodSpecializzazioneProfessionale)
        {
            var modelloPraticaSpecializzazioneProfessionale = await _context.ListaModelloPraticaSpecializzazioneProfessionale.Where(x => x.IdModelloPratica == IdModelloPratica && x.CodSpecializzazioneProfessionale == CodSpecializzazioneProfessionale).FirstOrDefaultAsync();

            if (modelloPraticaSpecializzazioneProfessionale == null)
            {
                return NotFound();
            }

            return modelloPraticaSpecializzazioneProfessionale;
        }
        
        // PUT: api/ModelloPraticaSpecializzazioneProfessionale/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{idModelloPratica}/{codSpecializzazioneProfessionale}")]
        public async Task<IActionResult> PutModelloPraticaSpecializzazioneProfessionale(int idModelloPratica, string codSpecializzazioneProfessionale, ModelloPraticaSpecializzazioneProfessionale modelloPraticaSpecializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo != "Sgrt" && utente.CodRuolo != "ITAd") return Unauthorized();

            if (idModelloPratica != modelloPraticaSpecializzazioneProfessionale.IdModelloPratica
                || codSpecializzazioneProfessionale != modelloPraticaSpecializzazioneProfessionale.CodSpecializzazioneProfessionale)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await modelloPraticaSpecializzazioneProfessionale.Update(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return AcceptedAtAction("PutModelloPraticaSpecializzazioneProfessionale", new { idModelloPratica = modelloPraticaSpecializzazioneProfessionale.IdModelloPratica, codSpecializzazioneProfessionale = modelloPraticaSpecializzazioneProfessionale.CodSpecializzazioneProfessionale }, await _context.ListaModelloPraticaSpecializzazioneProfessionale.Where(x => x.IdModelloPratica == modelloPraticaSpecializzazioneProfessionale.IdModelloPratica && x.CodSpecializzazioneProfessionale == modelloPraticaSpecializzazioneProfessionale.CodSpecializzazioneProfessionale).FirstOrDefaultAsync());
        }
        
        // POST: api/ModelloPraticaSpecializzazioneProfessionale
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ModelloPraticaSpecializzazioneProfessionale>> PostModelloPraticaSpecializzazioneProfessionale(ModelloPraticaSpecializzazioneProfessionale modelloPraticaSpecializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo != "Sgrt" && utente.CodRuolo != "ITAd") return Unauthorized();

            int result;
            try
            {
                result = await modelloPraticaSpecializzazioneProfessionale.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostModelloPraticaSpecializzazioneProfessionale", new { idModelloPratica = modelloPraticaSpecializzazioneProfessionale.IdModelloPratica, codSpecializzazioneProfessionale = modelloPraticaSpecializzazioneProfessionale.CodSpecializzazioneProfessionale }, await _context.ListaModelloPraticaSpecializzazioneProfessionale.Where(x => x.IdModelloPratica == modelloPraticaSpecializzazioneProfessionale.IdModelloPratica && x.CodSpecializzazioneProfessionale == modelloPraticaSpecializzazioneProfessionale.CodSpecializzazioneProfessionale).FirstOrDefaultAsync());
        }
        
        // DELETE: api/ModelloPraticaSpecializzazioneProfessionale/5
        [HttpDelete("{idModelloPratica}/{codSpecializzazioneProfessionale}")]
        public async Task<ActionResult<ModelloPraticaSpecializzazioneProfessionale>> DeleteModelloPraticaSpecializzazioneProfessionale(int idModelloPratica, string codSpecializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo != "Sgrt" && utente.CodRuolo != "ITAd") return Unauthorized();

            var modelloPraticaSpecializzazioneProfessionale = await _context.ModelloPraticaSpecializzazioneProfessionale.Where(x => x.IdModelloPratica == idModelloPratica && x.CodSpecializzazioneProfessionale == codSpecializzazioneProfessionale).FirstOrDefaultAsync();
            if (modelloPraticaSpecializzazioneProfessionale == null)
            {
                return NotFound();
            }

            int result;

            result = await modelloPraticaSpecializzazioneProfessionale.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool ModelloPraticaSpecializzazioneProfessionaleExists(int id)
        {
            return _context.ModelloPraticaSpecializzazioneProfessionale.Any(e => e.IdModelloPratica == id);
        }
    }
}
