﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;
using InformaTOSicuro.ModelExtension;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContattoPraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public ContattoPraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/ContattoPratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaContattoPratica>>> GetContattoPratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaContattoPratica
                    .Join(listaPraticaAmmessa, ListaContattoPratica => ListaContattoPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (ListaContattoPratica, listaPraticaAmmessa) => ListaContattoPratica)
                    .OrderByDescending(x => x.IdPratica)
                    .ThenBy(x => x.NumContatto).ToListAsync();
        }

        // GET: api/ContattoPratica/Pratica/1
        [HttpGet]
        [Route("Pratica/{idPratica}")]
        public async Task<ActionResult<IEnumerable<ListaContattoPratica>>> GetContattoPratica_Pratica(long idPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaContattoPratica
                    .Join(listaPraticaAmmessa, ListaContattoPratica => ListaContattoPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (ListaContattoPratica, listaPraticaAmmessa) => ListaContattoPratica)
                    .Where(x => x.IdPratica == idPratica)
                    .OrderBy(x => x.NumContatto).ToListAsync();
        }

        // GET: api/ContattoPratica/5
        [HttpGet("{idPratica}/{numContatto}")]
        public async Task<ActionResult<ListaContattoPratica>> GetContattoPratica(long idPratica, short numContatto)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);


            var contattoPratica = await _context.ListaContattoPratica
                .Join(listaPraticaAmmessa, ListaContattoPratica => ListaContattoPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (ListaContattoPratica, listaPraticaAmmessa) => ListaContattoPratica)
                .Where(x => x.IdPratica == idPratica && x.NumContatto == numContatto).FirstOrDefaultAsync();

            if (contattoPratica == null)
            {
                return NotFound();
            }

            return contattoPratica;
        }

        // GET: api/ContattoPratica/Guid/9FDA8358-B560-45D2-8009-62C4D7FD860F
        [HttpGet]
        [Route("Guid/{oneTimeToken}")]
        public async Task<ActionResult<ListaContattoPratica>> GetContattoPratica_Guid(Guid oneTimeToken)
        {

            var listaComunicazione = await _context.ListaComunicazione
                .Where(x => x.Uid == oneTimeToken).FirstOrDefaultAsync();

            if (listaComunicazione == null) return Unauthorized();
            
            var contattoPratica = await _context.ListaContattoPratica
                .Where(x => x.IdPratica == listaComunicazione.IdPratica && x.NumContatto == listaComunicazione.NumContatto).FirstOrDefaultAsync();

            if (contattoPratica == null)
            {
                return NotFound();
            }

            return contattoPratica;
        }

        // PUT: api/ContattoPratica/1/1
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{idPratica}/{numContatto}")]
        public async Task<IActionResult> PutContattoPratica(long idPratica, short numContatto, ContattoPratica contattoPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == idPratica).FirstAsync();

            if (praticaAmmessa == null) return Unauthorized();

            if (idPratica != contattoPratica.IdPratica || numContatto != contattoPratica.NumContatto)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await contattoPratica.Update(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return AcceptedAtAction("PutContattoPratica", new { idPratica = contattoPratica.IdPratica, numContatto = contattoPratica.NumContatto }, await _context.ListaContattoPratica.Where(x => x.IdPratica == contattoPratica.IdPratica && x.NumContatto == contattoPratica.NumContatto).FirstOrDefaultAsync());
        }

        // POST: api/ContattoPratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ContattoPratica>> PostContattoPratica(ContattoPratica contattoPratica)
        {
            int result;
            try
            {
                if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
                Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

                ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == contattoPratica.IdPratica).FirstOrDefaultAsync();

                if (praticaAmmessa == null) return Unauthorized();

                result = await contattoPratica.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostContattoPratica", new { idPratica = contattoPratica.IdPratica, numContatto = contattoPratica.NumContatto }, await _context.ListaContattoPratica.Where(x => x.IdPratica == contattoPratica.IdPratica && x.NumContatto == contattoPratica.NumContatto).FirstOrDefaultAsync());
        }

        // DELETE: api/ContattoPratica/1/1
        [HttpDelete("{idPratica}/{numContatto}")]
        public async Task<ActionResult<ContattoPratica>> DeleteContattoPratica(long idPratica, short numContatto)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == idPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            var contattoPratica = await _context.ContattoPratica.Where(x => x.IdPratica == idPratica && x.NumContatto == numContatto).FirstOrDefaultAsync();
            if (contattoPratica == null)
            {
                return NotFound();
            }

            int result;

            result = await contattoPratica.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool ContattoPraticaExists(long id)
        {
            return _context.ContattoPratica.Any(e => e.IdPratica == id);
        }
    }
}
