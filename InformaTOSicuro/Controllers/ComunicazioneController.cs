﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComunicazioneController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public ComunicazioneController(InformaTOSicuroContext context)
        {
            _context = context;
        }
        /*
        // GET: api/Comunicazione
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Comunicazione>>> GetComunicazione()
        {
            return await _context.Comunicazione.ToListAsync();
        }*/
        /*
        // GET: api/Comunicazione/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Comunicazione>> GetComunicazione(long id)
        {
            var comunicazione = await _context.Comunicazione.FindAsync(id);

            if (comunicazione == null)
            {
                return NotFound();
            }

            return comunicazione;
        }*/
        /*
        // PUT: api/Comunicazione/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComunicazione(long id, Comunicazione comunicazione)
        {
            if (id != comunicazione.Id)
            {
                return BadRequest();
            }

            _context.Entry(comunicazione).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComunicazioneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/
        /*
        // POST: api/Comunicazione
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Comunicazione>> PostComunicazione(Comunicazione comunicazione)
        {
            _context.Comunicazione.Add(comunicazione);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetComunicazione", new { id = comunicazione.Id }, comunicazione);
        }*/

        // POST: api/Comunicazione
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        [Route("ResetPassword")]
        public async Task<ActionResult<Comunicazione>> PostResetPassword(Comunicazione comunicazione)
        {
            int result;

            comunicazione.CodModello = "RsPw";
            comunicazione.CodCanale = "EMl ";
            comunicazione.Uid = Guid.NewGuid();

            var listaUtente = await _context.ListaUtente
                    .Where(x => x.Email == comunicazione.UsernameDestinatario
                        || x.Username == comunicazione.UsernameDestinatario).FirstOrDefaultAsync();

            if (listaUtente == null) return BadRequest();

            comunicazione.UsernameDestinatario = listaUtente.Username;
            comunicazione.EMail = listaUtente.Email;

            try
            {
                result = await comunicazione.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return Accepted();
        }

        /*
        // DELETE: api/Comunicazione/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Comunicazione>> DeleteComunicazione(long id)
        {
            var comunicazione = await _context.Comunicazione.FindAsync(id);
            if (comunicazione == null)
            {
                return NotFound();
            }

            _context.Comunicazione.Remove(comunicazione);
            await _context.SaveChangesAsync();

            return comunicazione;
        }*/

        private bool ComunicazioneExists(long id)
        {
            return _context.Comunicazione.Any(e => e.Id == id);
        }
    }
}
