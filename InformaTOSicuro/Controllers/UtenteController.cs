﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;
using System.Net.Http.Headers;
using System.Net;
using System.Text;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtenteController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public UtenteController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/Utente
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaUtente>>> GetUtente(
            [FromQuery] string cognomeNome,
            [FromQuery] bool? includiDisattivati,
            [FromQuery] string codRuolo,
            [FromQuery] string codStato
            )
        {

            IEnumerable<ListaUtente> listaUtente = await _context.ListaUtente
                .OrderBy(x => x.Cognome)
                .ThenBy(x => x.Nome)
                .ToListAsync();

            
            if (!string.IsNullOrEmpty(cognomeNome)) listaUtente = listaUtente
                    .Where(x => string.Concat(x.Nome, " ", x.Cognome).ToUpper().Contains(cognomeNome.ToUpper())
                        || string.Concat(x.Cognome, " ", x.Nome).ToUpper().Contains(cognomeNome.ToUpper())
                        || x.Username.ToUpper().Contains(cognomeNome.ToUpper())
                        || (x.Email ?? string.Empty).ToUpper().Contains(cognomeNome.ToUpper()));

            if (!string.IsNullOrEmpty(codRuolo)) listaUtente = listaUtente
                    .Where(x => x.CodRuolo == codRuolo);

            if (!string.IsNullOrEmpty(codStato)) listaUtente = listaUtente
                    .Where(x => x.CodStato == codStato);


            if (!(includiDisattivati ?? false)) listaUtente = listaUtente
                    .Where(x => x.CodStato != "Dstt");

            return Ok(listaUtente);
        }

        // GET: api/Utente/Logged
        [HttpGet]
        [Route("Logged")]
        public async Task<ActionResult<ListaUtente>> GetUtente_Logged()
        {

            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            ListaUtente listaUtente = await _context.ListaUtente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            listaUtente.ListaUtenteSpecializzazioneProfessionale = await _context.ListaUtenteSpecializzazioneProfessionale
                .Where(x => x.Username == listaUtente.Username).ToListAsync();

            return listaUtente;
            
        }

        // GET: api/Utente/MTognin
        [HttpGet("{username}")]
        public async Task<ActionResult<ListaUtente>> GetUtente(string username)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var utente = await _context.ListaUtente.Where(x => x.Username == username).FirstOrDefaultAsync();

            if (utente == null)
            {
                return NotFound();
            }

            utente.ListaUtenteSpecializzazioneProfessionale = await _context.ListaUtenteSpecializzazioneProfessionale
                .Where(x => x.Username == utente.Username).ToListAsync();

            return utente;
        }

        // PUT: api/Utente/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{username}")]
        public async Task<IActionResult> PutUtente(string username, Utente utente)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            if (username != utente.Username)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await utente.Update(_context);

                if (result == 2) return NotFound();

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch 
            {
                throw;
            }

            return AcceptedAtAction("PutUtente", new { id = utente.Username }, await _context.ListaUtente.Where(x => x.Username == utente.Username).FirstOrDefaultAsync());
        }

        // POST: api/Utente
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Utente>> PostUtente(Utente utente)
        {
            //if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            int result;
            try
            {
                result = await utente.Insert(_context, HttpContext.User.Identity.IsAuthenticated);

                if (result == 2) return Conflict();

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostUtente", new { id = utente.Username }, await _context.ListaUtente.Where(x => x.Username == utente.Username).FirstOrDefaultAsync());
        }

        // PATCH: api/Utente/MTognin
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPatch]
        [Route("SetPassword")]
        public async Task<IActionResult> PatchUtente_SetPassword(ModelExtension.CambioPassword cambioPassword)
        {

            Utente utente;

            if (HttpContext.User.Identity.IsAuthenticated) //return Unauthorized();
            { 
                utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();
            }
            else
            {
                if (cambioPassword.OneTimeToken == null) return Unauthorized();

                ListaComunicazione listaComunicazione = await _context.ListaComunicazione
                    .Where(x => x.Uid == cambioPassword.OneTimeToken
                        && x.DataInvio.Value.AddDays(3) >= DateTime.Now).FirstOrDefaultAsync();

                if (listaComunicazione == null) return Unauthorized();

                utente = await _context.Utente
                    .Where(x => x.Username == listaComunicazione.UsernameDestinatario).FirstOrDefaultAsync();

            }


            if (utente == null)
            {
                return BadRequest();
            }

            if (utente.Username != cambioPassword.Username && !string.IsNullOrEmpty(cambioPassword.Username)) return BadRequest();

            int result;

            try
            {
                result = await utente.SetPassword(_context, cambioPassword);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return Accepted();
        }

        [HttpPatch]
        [Route("Attivazione")]
        public async Task<IActionResult> PatchUtente_Attivazione(ModelExtension.AttivazioneUtente attivazioneUtente)
        {

            Utente utente;

            ListaComunicazione listaComunicazione = await _context.ListaComunicazione
                    .Where(x => x.Uid == attivazioneUtente.oneTimeToken).FirstOrDefaultAsync();

            if (listaComunicazione == null) return BadRequest();

            utente = await _context.Utente
                .Where(x => x.Username == listaComunicazione.UsernameDestinatario).FirstOrDefaultAsync();

            if (utente == null)
            {
                return BadRequest();
            }

            if (utente.CodStato != "DCnf")
            {
                return BadRequest();
            }

            int result;

            try
            {
                utente.CodStato = "Attv";
                result = await utente.SetStato(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return Accepted();
        }


        
        // DELETE: api/Utente/MTognin
        [HttpDelete("{username}")]
        public async Task<ActionResult<Utente>> DeleteUtente(string username)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente
                .Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (username != utente.Username)
            {
                return BadRequest();
            }

            if (utente == null)
            {
                return NotFound();
            }

            if (utente.CodStato != "Attv")
            {
                return BadRequest();
            }

            int result;

            try
            {
                utente.CodStato = "Dstt";
                result = await utente.SetStato(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return Accepted();
        }

        private bool UtenteExists(string id)
        {
            return _context.Utente.Any(e => e.Username == id);
        }
    }
}
