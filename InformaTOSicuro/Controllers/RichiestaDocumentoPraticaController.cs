﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;
using System.Security.Cryptography.X509Certificates;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RichiestaDocumentoPraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public RichiestaDocumentoPraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/RichiestaDocumentoPratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaRichiestaDocumentoPratica>>> GetRichiestaDocumentoPratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaRichiestaDocumentoPratica
                .Join(listaPraticaAmmessa, listaRichiestaDocumentoPratica => listaRichiestaDocumentoPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaRichiestaDocumentoPratica, listaPraticaAmmessa) => listaRichiestaDocumentoPratica)
                .OrderByDescending(x => x.IdPratica)
                .ThenBy(x => x.NumRichiesta).ToListAsync();
        }

        // GET: api/RichiestaDocumentoPratica/Pratica/5
        [HttpGet]
        [Route("Pratica/{idPratica}")]
        public async Task<ActionResult<IEnumerable<ListaRichiestaDocumentoPratica>>> GetRichiestaDocumentoPratica_Pratica(long idPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaRichiestaDocumentoPratica
                .Join(listaPraticaAmmessa, listaRichiestaDocumentoPratica => listaRichiestaDocumentoPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaRichiestaDocumentoPratica, listaPraticaAmmessa) => listaRichiestaDocumentoPratica)
                .Where(x => x.IdPratica == idPratica)
                .OrderBy(x => x.NumRichiesta).ToListAsync();
        }

        // GET: api/RichiestaDocumentoPratica/5
        [HttpGet("{idPratica}/{numRichiesta}")]
        public async Task<ActionResult<ListaRichiestaDocumentoPratica>> GetRichiestaDocumentoPratica(long idPratica, short numRichiesta)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            var richiestaDocumentoPratica = await _context.ListaRichiestaDocumentoPratica
                .Join(listaPraticaAmmessa, listaRichiestaDocumentoPratica => listaRichiestaDocumentoPratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaRichiestaDocumentoPratica, listaPraticaAmmessa) => listaRichiestaDocumentoPratica)
                .Where(x => x.IdPratica == idPratica && x.NumRichiesta == numRichiesta).FirstOrDefaultAsync();

            if (richiestaDocumentoPratica == null)
            {
                return NotFound();
            }

            return richiestaDocumentoPratica;
        }

        /*
        // PUT: api/RichiestaDocumentoPratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRichiestaDocumentoPratica(long id, RichiestaDocumentoPratica richiestaDocumentoPratica)
        {
            if (id != richiestaDocumentoPratica.IdPratica)
            {
                return BadRequest();
            }

            _context.Entry(richiestaDocumentoPratica).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RichiestaDocumentoPraticaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/RichiestaDocumentoPratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<RichiestaDocumentoPratica>> PostRichiestaDocumentoPratica(RichiestaDocumentoPratica richiestaDocumentoPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo == "Clnt") return Unauthorized();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == richiestaDocumentoPratica.IdPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();
            if (string.IsNullOrEmpty(richiestaDocumentoPratica.UsernameRichiedente)) richiestaDocumentoPratica.UsernameRichiedente = utente.Username;
            if (utente.Username != richiestaDocumentoPratica.UsernameRichiedente) return Unauthorized();

            int result;
            try
            {
                result = await richiestaDocumentoPratica.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostRichiestaDocumentoPratica", new { idPratica = richiestaDocumentoPratica.IdPratica, numRichiesta = richiestaDocumentoPratica.NumRichiesta }, await _context.ListaRichiestaDocumentoPratica.Where(x => x.IdPratica == richiestaDocumentoPratica.IdPratica && x.NumRichiesta == richiestaDocumentoPratica.NumRichiesta).FirstOrDefaultAsync());
        }

        // DELETE: api/RichiestaDocumentoPratica/5
        [HttpDelete("{idPratica}/{numRichiesta}")]
        public async Task<ActionResult<RichiestaDocumentoPratica>> DeleteRichiestaDocumentoPratica(long idPratica, short numRichiesta)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo == "Clnt") return Unauthorized();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == idPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            var richiestaDocumentoPratica = await _context.RichiestaDocumentoPratica.Where(x => x.IdPratica == idPratica && x.NumRichiesta == numRichiesta).FirstOrDefaultAsync();
            if (richiestaDocumentoPratica == null)
            {
                return NotFound();
            }

            int result;

            result = await richiestaDocumentoPratica.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool RichiestaDocumentoPraticaExists(long id)
        {
            return _context.RichiestaDocumentoPratica.Any(e => e.IdPratica == id);
        }
    }
}
