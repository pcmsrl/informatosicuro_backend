﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;
using System.Security.Cryptography.X509Certificates;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentoPraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public DocumentoPraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/DocumentoPratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaDocumentoPratica>>> GetDocumentoPratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await (from ldp in _context.ListaDocumentoPratica
                          select new ListaDocumentoPratica
                          {
                              IdPratica = ldp.IdPratica,
                              Titolo = ldp.Titolo,
                              NumDocumento = ldp.NumDocumento,
                              TipoFile = ldp.TipoFile,
                              NomeFile = ldp.NomeFile,
                              PercorsoFile = ldp.PercorsoFile,
                              CodTipoDocumento = ldp.CodTipoDocumento,
                              TipoDocumento = ldp.TipoDocumento,
                              UsernameInserimento = ldp.UsernameInserimento,
                              CognomeUtenteInserimento = ldp.CognomeUtenteInserimento,
                              NomeUtenteInserimento = ldp.NomeUtenteInserimento,
                              NumRichiesta = ldp.NumRichiesta,
                              DescrizioneRichiesta = ldp.DescrizioneRichiesta,
                              Note = ldp.Note,
                              DataInserimento = ldp.DataInserimento,
                              DocumentoBase64 = string.Empty
                          })
                          .Join(listaPraticaAmmessa, ldp => ldp.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (ldp, listaPraticaAmmessa) => ldp)
                          .OrderByDescending(x => x.IdPratica)
                          .ThenBy(x => x.NumDocumento).ToListAsync();

        }

        // GET: api/DocumentoPratica/Pratica/1
        [HttpGet]
        [Route("Pratica/{idPratica}")]
        public async Task<ActionResult<IEnumerable<ListaDocumentoPratica>>> GetDocumentoPratica_Pratica(long idPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await (from ldp in _context.ListaDocumentoPratica
                          where ldp.IdPratica == idPratica
                          select new ListaDocumentoPratica
                          {
                              IdPratica = ldp.IdPratica,
                              Titolo = ldp.Titolo,
                              NumDocumento = ldp.NumDocumento,
                              TipoFile = ldp.TipoFile,
                              NomeFile = ldp.NomeFile,
                              PercorsoFile = ldp.PercorsoFile,
                              CodTipoDocumento = ldp.CodTipoDocumento,
                              TipoDocumento = ldp.TipoDocumento,
                              UsernameInserimento = ldp.UsernameInserimento,
                              CognomeUtenteInserimento = ldp.CognomeUtenteInserimento,
                              NomeUtenteInserimento = ldp.NomeUtenteInserimento,
                              NumRichiesta = ldp.NumRichiesta,
                              DescrizioneRichiesta = ldp.DescrizioneRichiesta,
                              Note = ldp.Note,
                              DataInserimento = ldp.DataInserimento,
                              DocumentoBase64 = string.Empty
                          })
                          .Join(listaPraticaAmmessa, ldp => ldp.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (ldp, listaPraticaAmmessa) => ldp)
                          .OrderBy(x => x.NumDocumento).ToListAsync();

        }

        // GET: api/DocumentoPratica/5/2
        [HttpGet("{idPratica}/{numDocumento}")]
        public async Task<ActionResult<InformaTOSicuro.Model.ListaDocumentoPratica>> GetDocumentoPratica(long idPratica, short numDocumento)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            //var documentoPratica = await _context.ListaDocumentoPratica.Where(x => x.IdPratica == idPratica && x.NumDocumento == numDocumento).Select(x => new { x.IdPratica, x.Titolo, x.NumDocumento, x.TipoFile, x.NomeFile, x.PercorsoFile, x.CodTipoDocumento, x.DataInserimento, string.Empty }).FirstOrDefaultAsync();

            var documentoPratica = await (from ldp in _context.ListaDocumentoPratica
                                          where ldp.IdPratica == idPratica
                                            && ldp.NumDocumento == numDocumento
                                          select new ListaDocumentoPratica
                                          {
                                              IdPratica = ldp.IdPratica,
                                              Titolo = ldp.Titolo,
                                              NumDocumento = ldp.NumDocumento,
                                              TipoFile = ldp.TipoFile,
                                              NomeFile = ldp.NomeFile,
                                              PercorsoFile = ldp.PercorsoFile,
                                              CodTipoDocumento = ldp.CodTipoDocumento,
                                              TipoDocumento = ldp.TipoDocumento,
                                              UsernameInserimento = ldp.UsernameInserimento,
                                              CognomeUtenteInserimento = ldp.CognomeUtenteInserimento,
                                              NomeUtenteInserimento = ldp.NomeUtenteInserimento,
                                              NumRichiesta = ldp.NumRichiesta,
                                              DescrizioneRichiesta = ldp.DescrizioneRichiesta,
                                              Note = ldp.Note,
                                              DataInserimento = ldp.DataInserimento,
                                              DocumentoBase64 = string.Empty
                                          })
                                          .Join(listaPraticaAmmessa, ldp => ldp.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (ldp, listaPraticaAmmessa) => ldp)
                                          .FirstOrDefaultAsync();

            if (documentoPratica == null)
            {
                return NotFound();
            }

            documentoPratica.DocumentoBase64 = System.Convert.ToBase64String(System.IO.File.ReadAllBytes(documentoPratica.PercorsoFile));

            return documentoPratica;
        }
        /*
        // PUT: api/DocumentoPratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDocumentoPratica(long id, DocumentoPratica documentoPratica)
        {
            if (id != documentoPratica.IdPratica)
            {
                return BadRequest();
            }

            _context.Entry(documentoPratica).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DocumentoPraticaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/DocumentoPratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<DocumentoPratica>> PostDocumentoPratica(DocumentoPratica documentoPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == documentoPratica.IdPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            int result;
            try
            {
                result = await documentoPratica.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            var listaDocumentoPratica = await (from ldp in _context.ListaDocumentoPratica
                                               where ldp.IdPratica == documentoPratica.IdPratica
                                                && ldp.NumDocumento == documentoPratica.NumDocumento
                                               select new ListaDocumentoPratica
                                                   {
                                                       IdPratica = ldp.IdPratica,
                                                       Titolo = ldp.Titolo,
                                                       NumDocumento = ldp.NumDocumento,
                                                       TipoFile = ldp.TipoFile,
                                                       NomeFile = ldp.NomeFile,
                                                       PercorsoFile = ldp.PercorsoFile,
                                                       CodTipoDocumento = ldp.CodTipoDocumento,
                                                       TipoDocumento = ldp.TipoDocumento,
                                                       UsernameInserimento = ldp.UsernameInserimento,
                                                       CognomeUtenteInserimento = ldp.CognomeUtenteInserimento,
                                                       NomeUtenteInserimento = ldp.NomeUtenteInserimento,
                                                       NumRichiesta = ldp.NumRichiesta,
                                                       DescrizioneRichiesta = ldp.DescrizioneRichiesta,
                                                       Note = ldp.Note,
                                                       DataInserimento = ldp.DataInserimento,
                                                       DocumentoBase64 = string.Empty
                                                   }).FirstOrDefaultAsync();

            return CreatedAtAction("PostDocumentoPratica", new { idPratica = documentoPratica.IdPratica, numDocumento = documentoPratica.NumDocumento }, listaDocumentoPratica);
        }

        // DELETE: api/DocumentoPratica/5
        [HttpDelete("{idPratica}/{numDocumento}")]
        public async Task<ActionResult<DocumentoPratica>> DeleteDocumentoPratica(long idPratica,short numDocumento)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == idPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            var documentoPratica = await (from dp in _context.DocumentoPratica
                                          where dp.IdPratica == idPratica
                                            && dp.NumDocumento == numDocumento
                                          select new DocumentoPratica
                                          {
                                              IdPratica = dp.IdPratica,
                                              NumDocumento = dp.NumDocumento,
                                              TipoFile = dp.TipoFile,
                                              NomeFile = dp.NomeFile,
                                              PercorsoFile = dp.PercorsoFile,
                                              CodTipoDocumento = dp.CodTipoDocumento,
                                              UsernameInserimento = dp.UsernameInserimento,
                                              NumRichiesta = dp.NumRichiesta,
                                              Note = dp.Note,
                                              DataInserimento = dp.DataInserimento,
                                              DocumentoBase64 = string.Empty
                                          }).FirstOrDefaultAsync();

            if (documentoPratica == null)
            {
                return NotFound();
            }

            int result;

            result = await documentoPratica.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool DocumentoPraticaExists(long id)
        {
            return _context.DocumentoPratica.Any(e => e.IdPratica == id);
        }
    }
}
