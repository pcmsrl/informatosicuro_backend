﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PartecipanteRiunionePraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public PartecipanteRiunionePraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/PartecipanteRiunionePratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaPartecipanteRiunionePratica>>> GetPartecipanteRiunionePratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaPartecipanteRiunionePratica
                .Join(listaPraticaAmmessa, listaPartecipanteRiunionePratica => listaPartecipanteRiunionePratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaPartecipanteRiunionePratica, listaPraticaAmmessa) => listaPartecipanteRiunionePratica)
                .OrderByDescending(x => x.IdRiunionePratica)
                .ThenBy(x => x.CognomePartecipante)
                .ThenBy(x => x.NomePartecipante).ToListAsync();
        }

        // GET: api/PartecipanteRiunionePratica
        [HttpGet]
        [Route("RiunionePratica/{IdRiunionePratica}")]
        public async Task<ActionResult<IEnumerable<ListaPartecipanteRiunionePratica>>> GetPartecipanteRiunionePratica_RiunionePratica(long idRiunionePratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            return await _context.ListaPartecipanteRiunionePratica
                .Join(listaPraticaAmmessa, listaPartecipanteRiunionePratica => listaPartecipanteRiunionePratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaPartecipanteRiunionePratica, listaPraticaAmmessa) => listaPartecipanteRiunionePratica)
                .Where(x => x.IdRiunionePratica == idRiunionePratica)
                .OrderBy(x => x.CognomePartecipante)
                .ThenBy(x => x.NomePartecipante).ToListAsync();
        }

        // GET: api/PartecipanteRiunionePratica/1/MTognin
        [HttpGet("{IdRiunionePratica}/{UsernamePartecipante}")]
        public async Task<ActionResult<ListaPartecipanteRiunionePratica>> GetPartecipanteRiunionePratica(long IdRiunionePratica, string UsernamePartecipante)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            var partecipanteRiunionePratica = await _context.ListaPartecipanteRiunionePratica
                .Join(listaPraticaAmmessa, listaPartecipanteRiunionePratica => listaPartecipanteRiunionePratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaPartecipanteRiunionePratica, listaPraticaAmmessa) => listaPartecipanteRiunionePratica)
                .Where(x => x.IdRiunionePratica == IdRiunionePratica && x.UsernamePartecipante == UsernamePartecipante).FirstOrDefaultAsync();

            if (partecipanteRiunionePratica == null)
            {
                return NotFound();
            }

            return partecipanteRiunionePratica;
        }

        // PUT: api/PartecipanteRiunionePratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{idRiunionePratica}/{usernamePartecipante}")]
        public async Task<IActionResult> PutPartecipanteRiunionePratica(long idRiunionePratica, string usernamePartecipante, PartecipanteRiunionePratica partecipanteRiunionePratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            var listaPartecipanteRiunionePratica = await _context.ListaPartecipanteRiunionePratica
                .Join(listaPraticaAmmessa, listaPartecipanteRiunionePratica => listaPartecipanteRiunionePratica.IdPratica, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaPartecipanteRiunionePratica, listaPraticaAmmessa) => listaPartecipanteRiunionePratica)
                .Where(x => x.IdRiunionePratica == idRiunionePratica).ToListAsync();

            if (listaPartecipanteRiunionePratica == null) return Unauthorized();


            if (idRiunionePratica != partecipanteRiunionePratica.IdRiunionePratica || usernamePartecipante != partecipanteRiunionePratica.UsernamePartecipante)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await partecipanteRiunionePratica.Update(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return AcceptedAtAction("PutPartecipanteRiunionePratica", new { idRiunionePratica = partecipanteRiunionePratica.IdRiunionePratica, usernamePartecipante = partecipanteRiunionePratica.UsernamePartecipante }, await _context.ListaPartecipanteRiunionePratica.Where(x => x.IdRiunionePratica == partecipanteRiunionePratica.IdRiunionePratica && x.UsernamePartecipante == partecipanteRiunionePratica.UsernamePartecipante).FirstOrDefaultAsync());
        }

        // POST: api/PartecipanteRiunionePratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<PartecipanteRiunionePratica>> PostPartecipanteRiunionePratica(PartecipanteRiunionePratica partecipanteRiunionePratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            // manca controllo su pratica autorizzata per riunione

            int result;
            try
            {
                result = await partecipanteRiunionePratica.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostPartecipanteRiunionePratica", new { idRiunionePratica = partecipanteRiunionePratica.IdRiunionePratica, usernamePartecipante = partecipanteRiunionePratica.UsernamePartecipante }, await _context.ListaPartecipanteRiunionePratica.Where(x => x.IdRiunionePratica == partecipanteRiunionePratica.IdRiunionePratica && x.UsernamePartecipante == partecipanteRiunionePratica.UsernamePartecipante).FirstOrDefaultAsync());
        }

        // DELETE: api/PartecipanteRiunionePratica/5
        [HttpDelete("{idRiunionePratica}/{usernamePartecipante}")]
        public async Task<ActionResult<PartecipanteRiunionePratica>> DeletePartecipanteRiunionePratica(long idRiunionePratica, string usernamePartecipante)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            // manca controllo su autorizzazione per singola pratica

            var partecipanteRiunionePratica = await _context.PartecipanteRiunionePratica.Where(x => x.IdRiunionePratica == idRiunionePratica && x.UsernamePartecipante == usernamePartecipante).FirstOrDefaultAsync();
            if (partecipanteRiunionePratica == null)
            {
                return NotFound();
            }

            int result;

            result = await partecipanteRiunionePratica.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool PartecipanteRiunionePraticaExists(long id)
        {
            return _context.PartecipanteRiunionePratica.Any(e => e.IdRiunionePratica == id);
        }
    }
}
