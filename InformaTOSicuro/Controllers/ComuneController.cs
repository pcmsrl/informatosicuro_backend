﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComuneController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public ComuneController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/Comune
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Comune>>> GetComune()
        {
            return await _context.Comune.OrderBy(x => x.Comune1).ToListAsync();
        }

        // GET: api/Comune/Provincia/TO
        [HttpGet]
        [Route("Provincia/{siglaProvincia}")]
        public async Task<ActionResult<IEnumerable<Comune>>> GetComune_Provincia(string siglaProvincia)
        {
            return await _context.Comune.Where(x => x.SiglaProvincia == siglaProvincia).OrderBy(x => x.Comune1).ToListAsync();
        }

        // GET: api/Comune/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Comune>> GetComune(string id)
        {
            var comune = await _context.Comune.FindAsync(id);

            if (comune == null)
            {
                return NotFound();
            }

            return comune;
        }

        // PUT: api/Comune/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /*
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComune(string id, Comune comune)
        {
            if (id != comune.Cod)
            {
                return BadRequest();
            }

            _context.Entry(comune).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComuneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        */

        // POST: api/Comune
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /*
        [HttpPost]
        public async Task<ActionResult<Comune>> PostComune(Comune comune)
        {
            _context.Comune.Add(comune);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ComuneExists(comune.Cod))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetComune", new { id = comune.Cod }, comune);
        }
        */

        // DELETE: api/Comune/5
        /*
        [HttpDelete("{id}")]
        public async Task<ActionResult<Comune>> DeleteComune(string id)
        {
            var comune = await _context.Comune.FindAsync(id);
            if (comune == null)
            {
                return NotFound();
            }

            _context.Comune.Remove(comune);
            await _context.SaveChangesAsync();

            return comune;
        }
        */

        private bool ComuneExists(string id)
        {
            return _context.Comune.Any(e => e.Cod == id);
        }
    }
}
