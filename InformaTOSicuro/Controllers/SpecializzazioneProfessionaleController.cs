﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpecializzazioneProfessionaleController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public SpecializzazioneProfessionaleController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/SpecializzazioneProfessionale
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SpecializzazioneProfessionale>>> GetSpecializzazioneProfessionale(
            [FromQuery] int? IdModelloPratica,
            [FromQuery] int? IdPratica
            )
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            if (IdModelloPratica == null && IdPratica == null)
            {
                return await _context.SpecializzazioneProfessionale
                    .Where(x => x.DataCancellazione == null)
                    .OrderBy(x => x.Descrizione).ToListAsync();
            }

            if (IdModelloPratica != null && IdPratica != null) return BadRequest();

            if (IdModelloPratica != null)
            {
                var specializzazioneProfessionale = _context.SpecializzazioneProfessionale;

                var modelloPraticaSpecializzazioneProfessionale = _context.ModelloPraticaSpecializzazioneProfessionale
                    .Where(x => x.IdModelloPratica == IdModelloPratica);

                return await specializzazioneProfessionale
                    .Join(modelloPraticaSpecializzazioneProfessionale, specializzazioneProfessionale => specializzazioneProfessionale.Cod, modelloPraticaSpecializzazioneProfessionale => modelloPraticaSpecializzazioneProfessionale.CodSpecializzazioneProfessionale, (specializzazioneProfessionale, modelloPraticaSpecializzazioneProfessionale) => specializzazioneProfessionale)
                    .OrderBy(x => x.Descrizione).ToListAsync();

            }

            if (IdPratica != null)
            {
                var specializzazioneProfessionale = _context.SpecializzazioneProfessionale;

                var pratica = _context.Pratica
                    .Where(x => x.Id == IdPratica);

                var modelloPraticaSpecializzazioneProfessionale = _context.ModelloPraticaSpecializzazioneProfessionale;

                var praticaSpecializzazioneProfessionale = modelloPraticaSpecializzazioneProfessionale
                    .Join(pratica, modelloPraticaSpecializzazioneProfessionale => modelloPraticaSpecializzazioneProfessionale.IdModelloPratica, pratica => pratica.IdModello, (modelloPraticaSpecializzazioneProfessionale, pratica) => modelloPraticaSpecializzazioneProfessionale);

                return await specializzazioneProfessionale
                    .Join(praticaSpecializzazioneProfessionale, specializzazioneProfessionale => specializzazioneProfessionale.Cod, praticaSpecializzazioneProfessionale => praticaSpecializzazioneProfessionale.CodSpecializzazioneProfessionale, (specializzazioneProfessionale, praticaSpecializzazioneProfessionale) => specializzazioneProfessionale)
                    .OrderBy(x => x.Descrizione).ToListAsync();

            }

            return BadRequest();

        }

        // GET: api/SpecializzazioneProfessionale/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SpecializzazioneProfessionale>> GetSpecializzazioneProfessionale(string id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var specializzazioneProfessionale = await _context.SpecializzazioneProfessionale
                .Where(x => x.DataCancellazione == null)
                .Where(x => x.Cod == id).FirstOrDefaultAsync();

            if (specializzazioneProfessionale == null)
            {
                return NotFound();
            }

            return specializzazioneProfessionale;
        }
        
        // PUT: api/SpecializzazioneProfessionale/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{cod}")]
        public async Task<IActionResult> PutSpecializzazioneProfessionale(string cod, SpecializzazioneProfessionale specializzazioneProfessionale)
        {

            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            if (cod != specializzazioneProfessionale.Cod)
            {
                return BadRequest();
            }

            int result = await specializzazioneProfessionale.Update(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return AcceptedAtAction("GetSpecializzazioneProfessionale", new { Cod = specializzazioneProfessionale.Cod }, specializzazioneProfessionale);
        }

        // POST: api/SpecializzazioneProfessionale
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<SpecializzazioneProfessionale>> PostSpecializzazioneProfessionale(SpecializzazioneProfessionale specializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            int result;
            try
            {
                result = await specializzazioneProfessionale.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("GetSpecializzazioneProfessionale", new { Cod = specializzazioneProfessionale.Cod }, specializzazioneProfessionale);
        }

        // DELETE: api/SpecializzazioneProfessionale/5
        [HttpDelete("{cod}")]
        public async Task<ActionResult<SpecializzazioneProfessionale>> DeleteSpecializzazioneProfessionale(string cod)
        {

            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var specializzazioneProfessionale = await _context.SpecializzazioneProfessionale.FindAsync(cod);
            if (specializzazioneProfessionale == null)
            {
                return NotFound();
            }

            int result = await specializzazioneProfessionale.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();
        }

        private bool SpecializzazioneProfessionaleExists(string id)
        {
            return _context.SpecializzazioneProfessionale.Any(e => e.Cod == id);
        }
    }
}
