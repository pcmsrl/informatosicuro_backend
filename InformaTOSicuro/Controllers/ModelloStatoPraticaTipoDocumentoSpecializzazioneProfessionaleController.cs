﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;
using InformaTOSicuro.ModelExtension;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionaleController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionaleController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale>>> GetModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale()
        {
            return await _context.ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale
                .OrderBy(x => x.ModelloPratica)
                .ThenBy(x => x.TipoDocumento)
                .ThenBy(x => x.SpecializzazioneProfessionale).ToListAsync();
        }

        // GET: api/ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale/ModelloPratica/1
        [HttpGet]
        [Route("ModelloPratica/{idModello}")]
        public async Task<ActionResult<IEnumerable<ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale>>> GetModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale_ModelloPratica(int idModello)
        {
            //if (filtro == null)
            //{
                return await _context.ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale
                    .Where(x => x.IdModelloPratica == idModello)
                    .OrderBy(x => x.TipoDocumento)
                    .ThenBy(x => x.SpecializzazioneProfessionale).ToListAsync();
            /*}
            else
            {
                return await _context.ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale
                        .Where(x => x.IdModelloPratica == idModello
                            && (string.IsNullOrEmpty(filtro.CodStatoPratica) || x.CodStatoPratica == filtro.CodStatoPratica)
                            && (string.IsNullOrEmpty(filtro.CodSpecializzazioneProfessionale) || x.CodSpecializzazioneProfessionale == filtro.CodSpecializzazioneProfessionale))
                        .OrderBy(x => x.TipoDocumento)
                        .ThenBy(x => x.SpecializzazioneProfessionale)
                        .ToListAsync();
            }*/

        }

        // GET: api/ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale/5
        [HttpGet("{idModello}/{codStato}/{codTipoDocumento}/{codSpecializzazioneProfessionale}")]
        public async Task<ActionResult<ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale>> GetModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale(int idModello, string codStatoPratica, string codTipoDocumento, string CodSpecializzazioneProfessionale)
        {
            var modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale = await _context.ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.Where(x => x.IdModelloPratica == idModello && x.CodStatoPratica == codStatoPratica && x.CodTipoDocumento == codTipoDocumento && x.CodSpecializzazioneProfessionale == CodSpecializzazioneProfessionale).FirstOrDefaultAsync();

            if (modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale == null)
            {
                return NotFound();
            }

            return modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale;
        }
        /*
        // PUT: api/ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale(int id, ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale)
        {
            if (id != modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.IdModelloPratica)
            {
                return BadRequest();
            }

            _context.Entry(modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale>> PostModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale(ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo != "Sgrt" && utente.CodRuolo != "ITAd") return Unauthorized();

            int result;
            try
            {
                result = await modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale", new { idModelloPratica = modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.IdModelloPratica, codStatoPratica = modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.CodStatoPratica, codTipoDocumento = modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.CodTipoDocumento, codSpecializzazioneProfessionale = modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.CodSpecializzazioneProfessionale }, await _context.ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.Where(x => x.IdModelloPratica == modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.IdModelloPratica && x.CodStatoPratica == modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.CodStatoPratica && x.CodTipoDocumento == modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.CodTipoDocumento && x.CodSpecializzazioneProfessionale == modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.CodSpecializzazioneProfessionale).FirstOrDefaultAsync());
        }

        // DELETE: api/ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale/5
        [HttpDelete("{idModelloPratica}/{codStatoPratica}/{codTipoDocumento}/{codSpecializzazioneProfessionale}")]
        public async Task<ActionResult<ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale>> DeleteModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale(int idModelloPratica, string codStatoPratica, string codTipoDocumento, string codSpecializzazioneProfessionale)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo != "Sgrt" && utente.CodRuolo != "ITAd") return Unauthorized();

            var modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale = await _context.ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.Where(x => x.IdModelloPratica == idModelloPratica && x.CodStatoPratica == codStatoPratica && x.CodTipoDocumento == codTipoDocumento && x.CodSpecializzazioneProfessionale == codSpecializzazioneProfessionale).FirstOrDefaultAsync();
            if (modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale == null)
            {
                return NotFound();
            }

            int result;

            result = await modelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionaleExists(int id)
        {
            return _context.ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale.Any(e => e.IdModelloPratica == id);
        }
    }
}
