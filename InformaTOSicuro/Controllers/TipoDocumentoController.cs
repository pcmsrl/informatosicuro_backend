﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoDocumentoController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public TipoDocumentoController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/TipoDocumento
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoDocumento>>> GetTipoDocumento()
        {
            return await _context.TipoDocumento
                .Where(x => x.DataCancellazione == null)
                .OrderBy(x => x.Descrizione).ToListAsync();
        }

        // GET: api/TipoDocumento/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoDocumento>> GetTipoDocumento(string id)
        {
            var tipoDocumento = await _context.TipoDocumento
                .Where(x => x.DataCancellazione == null)
                .Where(x => x.Cod == id).FirstOrDefaultAsync();

            if (tipoDocumento == null)
            {
                return NotFound();
            }

            return tipoDocumento;
        }
        
        // PUT: api/TipoDocumento/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{cod}")]
        public async Task<IActionResult> PutTipoDocumento(string cod, TipoDocumento tipoDocumento)
        {

            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            if (cod != tipoDocumento.Cod)
            {
                return BadRequest();
            }

            int result = await tipoDocumento.Update(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return AcceptedAtAction("GetTipoDocumento", new { Cod = tipoDocumento.Cod }, tipoDocumento);

        }

        // POST: api/TipoDocumento
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TipoDocumento>> PostTipoDocumento(TipoDocumento tipoDocumento)
        {

            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            int result;
            try
            {
                result = await tipoDocumento.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("GetTipoDocumento", new { Cod = tipoDocumento.Cod }, tipoDocumento);
        }
        
        // DELETE: api/TipoDocumento/5
        [HttpDelete("{cod}")]
        public async Task<ActionResult<TipoDocumento>> DeleteTipoDocumento(string cod)
        {

            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var tipoDocumento = await _context.TipoDocumento.FindAsync(cod);
            if (tipoDocumento == null)
            {
                return NotFound();
            }

            int result = await tipoDocumento.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();
        }

        private bool TipoDocumentoExists(string id)
        {
            return _context.TipoDocumento.Any(e => e.Cod == id);
        }
    }
}
