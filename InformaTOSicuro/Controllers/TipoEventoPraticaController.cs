﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoEventoPraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public TipoEventoPraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/TipoEventoPratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoEventoPratica>>> GetTipoEventoPratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            
            return await _context.TipoEventoPratica
                .Where(x => x.SelezionabileUtente ?? false)
                .OrderBy(x => x.Descrizione).ToListAsync();
        }

        // GET: api/TipoEventoPratica/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoEventoPratica>> GetTipoEventoPratica(string id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var tipoEventoPratica = await _context.TipoEventoPratica.FindAsync(id);

            if (tipoEventoPratica == null)
            {
                return NotFound();
            }

            return tipoEventoPratica;
        }
        /*
        // PUT: api/TipoEventoPratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTipoEventoPratica(string id, TipoEventoPratica tipoEventoPratica)
        {
            if (id != tipoEventoPratica.Cod)
            {
                return BadRequest();
            }

            _context.Entry(tipoEventoPratica).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoEventoPraticaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/
        /*
        // POST: api/TipoEventoPratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TipoEventoPratica>> PostTipoEventoPratica(TipoEventoPratica tipoEventoPratica)
        {
            _context.TipoEventoPratica.Add(tipoEventoPratica);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TipoEventoPraticaExists(tipoEventoPratica.Cod))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTipoEventoPratica", new { id = tipoEventoPratica.Cod }, tipoEventoPratica);
        }*/
        /*
        // DELETE: api/TipoEventoPratica/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TipoEventoPratica>> DeleteTipoEventoPratica(string id)
        {
            var tipoEventoPratica = await _context.TipoEventoPratica.FindAsync(id);
            if (tipoEventoPratica == null)
            {
                return NotFound();
            }

            _context.TipoEventoPratica.Remove(tipoEventoPratica);
            await _context.SaveChangesAsync();

            return tipoEventoPratica;
        }*/

        private bool TipoEventoPraticaExists(string id)
        {
            return _context.TipoEventoPratica.Any(e => e.Cod == id);
        }
    }
}
