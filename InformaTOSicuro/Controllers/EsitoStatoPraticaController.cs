﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EsitoStatoPraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public EsitoStatoPraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/EsitoStatoPratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EsitoStatoPratica>>> GetEsitoStatoPratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            return await _context.EsitoStatoPratica
                .OrderBy(x => x.Descrizione).ToListAsync();
        }

        // GET: api/EsitoStatoPratica/Lvrz/Annl
        [HttpGet("{CodStato}/{CodEsito}")]
        public async Task<ActionResult<EsitoStatoPratica>> GetEsitoStatoPratica(string CodStato, string CodEsito)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var esitoStatoPratica = await _context.EsitoStatoPratica.Where(x => x.CodStato == CodStato && x.CodEsito == CodEsito).FirstOrDefaultAsync();

            if (esitoStatoPratica == null)
            {
                return NotFound();
            }

            return esitoStatoPratica;
        }

        // PUT: api/EsitoStatoPratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /*
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEsitoStatoPratica(string id, EsitoStatoPratica esitoStatoPratica)
        {
            if (id != esitoStatoPratica.CodStato)
            {
                return BadRequest();
            }

            _context.Entry(esitoStatoPratica).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EsitoStatoPraticaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        */

        // POST: api/EsitoStatoPratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /*
        [HttpPost]
        public async Task<ActionResult<EsitoStatoPratica>> PostEsitoStatoPratica(EsitoStatoPratica esitoStatoPratica)
        {
            _context.EsitoStatoPratica.Add(esitoStatoPratica);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EsitoStatoPraticaExists(esitoStatoPratica.CodStato))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEsitoStatoPratica", new { id = esitoStatoPratica.CodStato }, esitoStatoPratica);
        }
        */

        // DELETE: api/EsitoStatoPratica/5
        /*
        [HttpDelete("{id}")]
        public async Task<ActionResult<EsitoStatoPratica>> DeleteEsitoStatoPratica(string id)
        {
            var esitoStatoPratica = await _context.EsitoStatoPratica.FindAsync(id);
            if (esitoStatoPratica == null)
            {
                return NotFound();
            }

            _context.EsitoStatoPratica.Remove(esitoStatoPratica);
            await _context.SaveChangesAsync();

            return esitoStatoPratica;
        }
        */

        private bool EsitoStatoPraticaExists(string id)
        {
            return _context.EsitoStatoPratica.Any(e => e.CodStato == id);
        }
    }
}
