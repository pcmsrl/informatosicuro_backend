﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;
using InformaTOSicuro.ModelExtension;
using Microsoft.EntityFrameworkCore.Internal;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public PraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/Pratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaPratica>>> GetPratica([FromQuery] Filtro_Pratica filtroPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            List<ListaPratica> listaPratica;

            if (filtroPratica == null)
            {
                
                listaPratica = await _context.ListaPratica.Join(listaPraticaAmmessa,listaPratica => listaPratica.Id, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaPratica, listaPraticaAmmessa) => listaPratica)
                    .Join(listaPraticaAmmessa, listaPratica => listaPratica.Id, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaPratica, listaPraticaAmmessa) => listaPratica)
                    .OrderByDescending(x => x.Id).ToListAsync();

            }
            else
            {

                if (filtroPratica.DataRichiestaDa == DateTime.MinValue) filtroPratica.DataRichiestaDa = new DateTime(1970, 1, 1);
                if (filtroPratica.DataRichiestaA == DateTime.MinValue) filtroPratica.DataRichiestaA = new DateTime(9999,12,31);

                listaPratica = await _context.ListaPratica.Join(listaPraticaAmmessa, listaPratica => listaPratica.Id, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaPratica, listaPraticaAmmessa) => listaPratica)
                    .Where(x => string.IsNullOrEmpty(filtroPratica.CodStatoPratica) || x.CodStatoPratica == filtroPratica.CodStatoPratica)
                    .Where(x => !filtroPratica.SoloChiusa || x.CodStatoPratica == "Chs ")
                    .Where(x => !filtroPratica.SoloAperta || x.CodStatoPratica != "Chs ")
                    .Where(x => x.Richiedente.Contains(filtroPratica.Richiedente) || string.IsNullOrEmpty(filtroPratica.Richiedente))
                    .Where(x => x.DataRichiesta >= filtroPratica.DataRichiestaDa || filtroPratica.DataRichiestaDa == new DateTime(1970, 1, 1))
                    .Where(x => x.DataRichiesta <= filtroPratica.DataRichiestaA || filtroPratica.DataRichiestaA == new DateTime(9999, 12, 31))
                    .Where(x => x.UsernameResponsabile == filtroPratica.UsernameResponsabile || string.IsNullOrEmpty(filtroPratica.UsernameResponsabile))
                    .Where(x => x.UsernameSegretaria == filtroPratica.UsernameSegretaria || string.IsNullOrEmpty(filtroPratica.UsernameSegretaria) || x.UsernameSegretaria == null)
                    .OrderByDescending(x => x.Id)
                    .ToListAsync();
            }
            
            foreach (var lp in listaPratica)
            {
                lp.EsitoStatoPraticaAmmesso = _context.EsitoStatoPratica.Where(x => x.CodStato == lp.CodStatoPratica).ToList();
            };
            
            return listaPratica;


        }

        // GET: api/Pratica/5
        [HttpGet("{Id}")]
        public async Task<ActionResult<ListaPratica>> GetPratica(long Id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            IQueryable<ListaPratica> listaPraticaAmmessa = utente.ListaPratica(_context);

            var pratica = await _context.ListaPratica.Join(listaPraticaAmmessa, listaPratica => listaPratica.Id, listaPraticaAmmessa => listaPraticaAmmessa.Id, (listaPratica, listaPraticaAmmessa) => listaPratica)
                .Where(x => x.Id == Id).FirstOrDefaultAsync();

            if (pratica == null)
            {
                return NotFound();
            }

            pratica.EsitoStatoPraticaAmmesso = _context.EsitoStatoPratica.Where(x => x.CodStato == pratica.CodStatoPratica).ToList();

            return pratica;
        }

        // PUT: api/Pratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPratica(long id, Pratica pratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == id).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            if (id != pratica.Id)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await pratica.Update(_context, utente.Username);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return AcceptedAtAction("PutPratica", new { id = pratica.Id }, await _context.ListaPratica.Where(x => x.Id == pratica.Id).FirstOrDefaultAsync());
        }

        // PATCH: api/Pratica/1/EsitoStato/Annl
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPatch]
        [Route("{idPratica}/EsitoStato/{esitoStato}")]
        public async Task<IActionResult> PatchPratica_EsitoStato(long idPratica, string esitoStato)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            ListaPratica praticaAmmessa = await utente.ListaPratica(_context).Where(x => x.Id == idPratica).FirstOrDefaultAsync();

            if (praticaAmmessa == null) return Unauthorized();

            Pratica pratica = await _context.Pratica.FindAsync(idPratica);
            
            if (pratica == null)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await pratica.SetEsitoStato(_context,esitoStato);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return AcceptedAtAction("PatchPratica_EsitoStato", new { idPratica = pratica.Id, esitoStato }, await _context.ListaPratica.Where(x => x.Id == idPratica).FirstOrDefaultAsync());
        }

        // POST: api/Pratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Pratica>> PostPratica(Pratica pratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();


            int result;
            try
            {
                result = await pratica.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostPratica", new { id = pratica.Id }, await _context.ListaPratica.Where(x => x.Id == pratica.Id).FirstOrDefaultAsync());
        }

        /*
        // DELETE: api/Pratica/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Pratica>> DeletePratica(long id)
        {
            var pratica = await _context.Pratica.FindAsync(id);
            if (pratica == null)
            {
                return NotFound();
            }

            _context.Pratica.Remove(pratica);
            await _context.SaveChangesAsync();

            return pratica;
        }*/

        private bool PraticaExists(long id)
        {
            return _context.Pratica.Any(e => e.Id == id);
        }
    }
}
