﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatoPraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public StatoPraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/StatoPratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StatoPratica>>> GetStatoPratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            return await _context.StatoPratica
                .OrderBy(x => x.Descrizione).ToListAsync();
        }

        // GET: api/StatoPratica/5
        [HttpGet("{id}")]
        public async Task<ActionResult<StatoPratica>> GetStatoPratica(string id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var statoPratica = await _context.StatoPratica.FindAsync(id);

            if (statoPratica == null)
            {
                return NotFound();
            }

            return statoPratica;
        }

        // PUT: api/StatoPratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStatoPratica(string id, StatoPratica statoPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            if (id != statoPratica.Cod)
            {
                return BadRequest();
            }

            _context.Entry(statoPratica).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StatoPraticaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/StatoPratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<StatoPratica>> PostStatoPratica(StatoPratica statoPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            _context.StatoPratica.Add(statoPratica);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (StatoPraticaExists(statoPratica.Cod))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetStatoPratica", new { id = statoPratica.Cod }, statoPratica);
        }

        // DELETE: api/StatoPratica/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<StatoPratica>> DeleteStatoPratica(string id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var statoPratica = await _context.StatoPratica.FindAsync(id);
            if (statoPratica == null)
            {
                return NotFound();
            }

            _context.StatoPratica.Remove(statoPratica);
            await _context.SaveChangesAsync();

            return statoPratica;
        }

        private bool StatoPraticaExists(string id)
        {
            return _context.StatoPratica.Any(e => e.Cod == id);
        }
    }
}
