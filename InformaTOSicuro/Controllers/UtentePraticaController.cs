﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtentePraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public UtentePraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/UtentePratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaUtentePratica>>> GetUtentePratica()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            return await _context.ListaUtentePratica
                .OrderByDescending(x => x.IdPratica)
                .ThenBy(x => x.CognomeUtente)
                .ThenBy(x => x.NomeUtente).ToListAsync();
        }

        // GET: api/UtentePratica
        [HttpGet]
        [Route("Pratica/{id}")]
        public async Task<ActionResult<IEnumerable<ListaUtentePratica>>> GetUtentePratica_Pratica(long id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            return await _context.ListaUtentePratica
                .Where(x => x.IdPratica == id)
                .OrderBy(x => x.CognomeUtente)
                .ThenBy(x => x.NomeUtente).ToListAsync();
        }

        // GET: api/UtentePratica/5
        [HttpGet("{idPratica}/{username}")]
        public async Task<ActionResult<ListaUtentePratica>> GetUtentePratica(long idPratica, string username)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var utentePratica = await _context.ListaUtentePratica.Where(x => x.IdPratica == idPratica && x.Username == username).FirstOrDefaultAsync();

            if (utentePratica == null)
            {
                return NotFound();
            }

            return utentePratica;
        }

        // PUT: api/UtentePratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{idPratica}/{username}")]
        public async Task<IActionResult> PutUtentePratica(long idPratica, string username, UtentePratica utentePratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            if (idPratica != utentePratica.IdPratica || username != utentePratica.Username)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await utentePratica.Update(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return AcceptedAtAction("PutUtentePratica", new { idPratica = utentePratica.IdPratica, username = utentePratica.Username }, await _context.ListaUtentePratica.Where(x => x.IdPratica == utentePratica.IdPratica && x.Username == utentePratica.Username).FirstOrDefaultAsync());
        }

        // POST: api/UtentePratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<UtentePratica>> PostUtentePratica(UtentePratica utentePratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            int result;
            try
            {
                result = await utentePratica.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostUtentePratica", new { idPratica = utentePratica.IdPratica, username = utentePratica.Username }, await _context.ListaUtentePratica.Where(x => x.IdPratica == utentePratica.IdPratica && x.Username == utentePratica.Username).FirstOrDefaultAsync());
        }

        // DELETE: api/UtentePratica/5
        [HttpDelete("{idPratica}/{username}")]
        public async Task<ActionResult<UtentePratica>> DeleteUtentePratica(long idPratica, string username)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var utentePratica = await _context.UtentePratica.Where(x => x.IdPratica == idPratica && x.Username == username).FirstOrDefaultAsync();
            if (utentePratica == null)
            {
                return NotFound();
            }

            int result;

            result = await utentePratica.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool UtentePraticaExists(long id)
        {
            return _context.UtentePratica.Any(e => e.IdPratica == id);
        }
    }
}
