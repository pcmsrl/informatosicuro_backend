﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssociazioneController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public AssociazioneController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/Associazione
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaAssociazione>>> GetAssociazione()
        {
            return await _context.ListaAssociazione
                .OrderBy(x => x.RagioneSociale).ToListAsync();
        }

        // GET: api/Associazione/5
        [HttpGet("{Id}")]
        public async Task<ActionResult<ListaAssociazione>> GetAssociazione(int Id)
        {
            var associazione = await _context.ListaAssociazione.Where(la => la.Id == Id).FirstOrDefaultAsync();

            if (associazione == null)
            {
                return NotFound();
            }

            return associazione;
        }

        // PUT: api/Associazione/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /*
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAssociazione(int id, Associazione associazione)
        {
            if (id != associazione.Id)
            {
                return BadRequest();
            }

            _context.Entry(associazione).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssociazioneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        */

        // POST: api/Associazione
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /*
        [HttpPost]
        public async Task<ActionResult<Associazione>> PostAssociazione(Associazione associazione)
        {
            _context.Associazione.Add(associazione);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAssociazione", new { id = associazione.Id }, associazione);
        }
        */

        // DELETE: api/Associazione/5
        /*
        [HttpDelete("{id}")]
        public async Task<ActionResult<Associazione>> DeleteAssociazione(int id)
        {
            var associazione = await _context.Associazione.FindAsync(id);
            if (associazione == null)
            {
                return NotFound();
            }

            _context.Associazione.Remove(associazione);
            await _context.SaveChangesAsync();

            return associazione;
        }
        */

        private bool AssociazioneExists(int id)
        {
            return _context.Associazione.Any(e => e.Id == id);
        }
    }
        
}
