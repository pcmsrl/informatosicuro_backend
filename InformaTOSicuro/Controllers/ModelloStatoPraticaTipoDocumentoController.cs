﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModelloStatoPraticaTipoDocumentoController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public ModelloStatoPraticaTipoDocumentoController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/ModelloStatoPraticaTipoDocumento
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaModelloStatoPraticaTipoDocumento>>> GetModelloStatoPraticaTipoDocumento()
        {
            return await _context.ListaModelloStatoPraticaTipoDocumento
                .OrderBy(x => x.ModelloPratica)
                .ThenBy(x => x.TipoDocumento).ToListAsync();
        }

        // GET: api/ModelloStatoPraticaTipoDocumento/1/Istr/CIdn
        [HttpGet("{IdModelloPratica}/{CodStatoPratica}/{CodTipoDocumento}")]
        public async Task<ActionResult<ListaModelloStatoPraticaTipoDocumento>> GetModelloStatoPraticaTipoDocumento(int IdModelloPratica, string CodStatoPratica, string CodTipoDocumento)
        {
            var modelloStatoPraticaTipoDocumento = await _context.ListaModelloStatoPraticaTipoDocumento
                .Where(x => x.IdModelloPratica == IdModelloPratica && x.CodStatoPratica == CodStatoPratica && x.CodTipoDocumento == CodTipoDocumento)
                .OrderBy(x => x.TipoDocumento).FirstOrDefaultAsync();

            if (modelloStatoPraticaTipoDocumento == null)
            {
                return NotFound();
            }

            return modelloStatoPraticaTipoDocumento;
        }
        /*
        // PUT: api/ModelloStatoPraticaTipoDocumento/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutModelloStatoPraticaTipoDocumento(int id, ModelloStatoPraticaTipoDocumento modelloStatoPraticaTipoDocumento)
        {
            if (id != modelloStatoPraticaTipoDocumento.IdModelloPratica)
            {
                return BadRequest();
            }

            _context.Entry(modelloStatoPraticaTipoDocumento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModelloStatoPraticaTipoDocumentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/
        /*
        // POST: api/ModelloStatoPraticaTipoDocumento
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ModelloStatoPraticaTipoDocumento>> PostModelloStatoPraticaTipoDocumento(ModelloStatoPraticaTipoDocumento modelloStatoPraticaTipoDocumento)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo == "Clnt") return Unauthorized();

            int result;
            try
            {
                result = await modelloStatoPraticaTipoDocumento.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostModelloStatoPraticaTipoDocumento", new { idModelloPratica = modelloStatoPraticaTipoDocumento.IdModelloPratica, codStatoPratica = modelloStatoPraticaTipoDocumento.CodStatoPratica, codTipoDocumento = modelloStatoPraticaTipoDocumento.CodTipoDocumento }, await _context.ListaModelloPraticaSpecializzazioneProfessionale.Where(x => x.IdModelloPratica == modelloPraticaSpecializzazioneProfessionale.IdModelloPratica && x.CodSpecializzazioneProfessionale == modelloPraticaSpecializzazioneProfessionale.CodSpecializzazioneProfessionale).FirstOrDefaultAsync());
        }*/
        /*
        // DELETE: api/ModelloStatoPraticaTipoDocumento/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ModelloStatoPraticaTipoDocumento>> DeleteModelloStatoPraticaTipoDocumento(int id)
        {
            var modelloStatoPraticaTipoDocumento = await _context.ModelloStatoPraticaTipoDocumento.FindAsync(id);
            if (modelloStatoPraticaTipoDocumento == null)
            {
                return NotFound();
            }

            _context.ModelloStatoPraticaTipoDocumento.Remove(modelloStatoPraticaTipoDocumento);
            await _context.SaveChangesAsync();

            return modelloStatoPraticaTipoDocumento;
        }*/

        private bool ModelloStatoPraticaTipoDocumentoExists(int id)
        {
            return _context.ModelloStatoPraticaTipoDocumento.Any(e => e.IdModelloPratica == id);
        }
    }
}
