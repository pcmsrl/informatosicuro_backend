﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModelloPraticaController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public ModelloPraticaController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/ModelloPratica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaModelloPratica>>> GetModelloPratica()
        {
            List<ListaModelloPratica> listaModelloPratica = await _context.ListaModelloPratica
                .OrderBy(x => x.Titolo)
                .ThenBy(x => x.Descrizione).ToListAsync();

            foreach (ListaModelloPratica modelloPratica in listaModelloPratica)
            {

                modelloPratica.ListaModelloPraticaSpecializzazioneProfessionale = await _context.ListaModelloPraticaSpecializzazioneProfessionale
                    .Where(x => x.IdModelloPratica == modelloPratica.Id)
                    .OrderBy(x => x.SpecializzazioneProfessionale).ToListAsync();
                modelloPratica.ListaModelloStatoPraticaTipoDocumentoTipoDestinatario = await _context.ListaModelloStatoPraticaTipoDocumentoTipoDestinatario
                    .Where(x => x.IdModelloPratica == modelloPratica.Id)
                    .OrderBy(x => x.TipoDocumento)
                    .ThenBy(x => x.TipoDestinatario).ToListAsync();

            }

            return listaModelloPratica;
        }

        // GET: api/ModelloPratica/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ListaModelloPratica>> GetModelloPratica(int Id)
        {
            var modelloPratica = await _context.ListaModelloPratica.Where(x => x.Id == Id).FirstOrDefaultAsync();

            if (modelloPratica == null)
            {
                return NotFound();
            }

            modelloPratica.ListaModelloPraticaSpecializzazioneProfessionale = await _context.ListaModelloPraticaSpecializzazioneProfessionale.Where(x => x.IdModelloPratica == modelloPratica.Id).ToListAsync();
            modelloPratica.ListaModelloStatoPraticaTipoDocumentoTipoDestinatario = await _context.ListaModelloStatoPraticaTipoDocumentoTipoDestinatario.Where(x => x.IdModelloPratica == modelloPratica.Id).ToListAsync();

            return modelloPratica;
        }
        
        // PUT: api/ModelloPratica/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutModelloPratica(int id, ModelloPratica modelloPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo != "Sgrt" && utente.CodRuolo != "ITAd") return Unauthorized();

            if (id != modelloPratica.Id)
            {
                return BadRequest();
            }

            int result;

            try
            {
                result = await modelloPratica.Update(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return AcceptedAtAction("PutModelloPratica", new { id = modelloPratica.Id }, await _context.ListaModelloPratica.Where(x => x.Id == modelloPratica.Id).FirstOrDefaultAsync());
        }
        
        // POST: api/ModelloPratica
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ModelloPratica>> PostModelloPratica(ModelloPratica modelloPratica)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo != "Sgrt" && utente.CodRuolo != "ITAd") return Unauthorized();

            if (utente.Username != modelloPratica.UsernameCreazione) return BadRequest();

            int result;
            try
            {
                result = await modelloPratica.Insert(_context);

                if (result > 0) return UnprocessableEntity(new { internalCode = result });

            }
            catch
            {
                throw;
            }

            return CreatedAtAction("PostModelloPratica", new { id = modelloPratica.Id }, await _context.ListaModelloPratica.Where(x => x.Id == modelloPratica.Id).FirstOrDefaultAsync());
        }

        
        // DELETE: api/ModelloPratica/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ModelloPratica>> DeleteModelloPratica(int id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            Utente utente = await _context.Utente.Where(x => x.Username == HttpContext.User.Identity.AuthenticationType).FirstOrDefaultAsync();

            if (utente.CodRuolo != "Sgrt" && utente.CodRuolo != "ITAd") return Unauthorized();

            var modelloPratica = await _context.ModelloPratica.FindAsync(id);
            if (modelloPratica == null)
            {
                return NotFound();
            }

            int result;

            result = await modelloPratica.Delete(_context);

            if (result > 0) return UnprocessableEntity(new { internalCode = result });

            return Accepted();

        }

        private bool ModelloPraticaExists(int id)
        {
            return _context.ModelloPratica.Any(e => e.Id == id);
        }
    }
}
