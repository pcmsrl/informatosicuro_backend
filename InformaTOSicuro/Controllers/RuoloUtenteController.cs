﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RuoloUtenteController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public RuoloUtenteController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/RuoloUtente
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RuoloUtente>>> GetRuoloUtente()
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            return await _context.RuoloUtente
                .OrderBy(x => x.Descrizione).ToListAsync();
        }

        // GET: api/RuoloUtente/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RuoloUtente>> GetRuoloUtente(string id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var ruoloUtente = await _context.RuoloUtente.FindAsync(id);

            if (ruoloUtente == null)
            {
                return NotFound();
            }

            return ruoloUtente;
        }

        // PUT: api/RuoloUtente/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRuoloUtente(string id, RuoloUtente ruoloUtente)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            if (id != ruoloUtente.Cod)
            {
                return BadRequest();
            }

            _context.Entry(ruoloUtente).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RuoloUtenteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RuoloUtente
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<RuoloUtente>> PostRuoloUtente(RuoloUtente ruoloUtente)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            _context.RuoloUtente.Add(ruoloUtente);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RuoloUtenteExists(ruoloUtente.Cod))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetRuoloUtente", new { id = ruoloUtente.Cod }, ruoloUtente);
        }

        // DELETE: api/RuoloUtente/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RuoloUtente>> DeleteRuoloUtente(string id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var ruoloUtente = await _context.RuoloUtente.FindAsync(id);
            if (ruoloUtente == null)
            {
                return NotFound();
            }

            _context.RuoloUtente.Remove(ruoloUtente);
            await _context.SaveChangesAsync();

            return ruoloUtente;
        }

        private bool RuoloUtenteExists(string id)
        {
            return _context.RuoloUtente.Any(e => e.Cod == id);
        }
    }
}
