﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InformaTOSicuro.Model;

namespace InformaTOSicuro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtenteAssociazioneController : ControllerBase
    {
        private readonly InformaTOSicuroContext _context;

        public UtenteAssociazioneController(InformaTOSicuroContext context)
        {
            _context = context;
        }

        // GET: api/UtenteAssociazione
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ListaUtenteAssociazione>>> GetUtenteAssociazione()
        {
            return await _context.ListaUtenteAssociazione
                .OrderBy(x => x.Associazione)
                .ThenBy(x => x.CognomeProfessionista)
                .ThenBy(x => x.NomeProfessionista).ToListAsync();
        }

        // GET: api/UtenteAssociazione/5
        [HttpGet("{usernameProfessionista}/{idAssociazione}")]
        public async Task<ActionResult<ListaUtenteAssociazione>> GetUtenteAssociazione(string usernameProfessionista, int idAssociazione)
        {
            var utenteAssociazione = await _context.ListaUtenteAssociazione.Where(x => x.UsernameProfessionista == usernameProfessionista && x.IdAssociazione == idAssociazione).FirstOrDefaultAsync();

            if (utenteAssociazione == null)
            {
                return NotFound();
            }

            return utenteAssociazione;
        }

        // PUT: api/UtenteAssociazione/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUtenteAssociazione(string id, UtenteAssociazione utenteAssociazione)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            if (id != utenteAssociazione.Username)
            {
                return BadRequest();
            }

            _context.Entry(utenteAssociazione).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UtenteAssociazioneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UtenteAssociazione
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<UtenteAssociazione>> PostUtenteAssociazione(UtenteAssociazione utenteAssociazione)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();
            
            _context.UtenteAssociazione.Add(utenteAssociazione);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UtenteAssociazioneExists(utenteAssociazione.Username))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUtenteAssociazione", new { id = utenteAssociazione.Username }, utenteAssociazione);
        }

        // DELETE: api/UtenteAssociazione/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UtenteAssociazione>> DeleteUtenteAssociazione(string id)
        {
            if (!HttpContext.User.Identity.IsAuthenticated) return Unauthorized();

            var utenteAssociazione = await _context.UtenteAssociazione.FindAsync(id);
            if (utenteAssociazione == null)
            {
                return NotFound();
            }

            _context.UtenteAssociazione.Remove(utenteAssociazione);
            await _context.SaveChangesAsync();

            return utenteAssociazione;
        }

        private bool UtenteAssociazioneExists(string id)
        {
            return _context.UtenteAssociazione.Any(e => e.Username == id);
        }
    }
}
