﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class ModelloStatoPraticaTipoDocumentoTipoDestinatario
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdModelloPratica", this.IdModelloPratica),
                new SqlParameter("@CodStatoPratica", this.CodStatoPratica),
                new SqlParameter("@CodTipoDocumento", this.CodTipoDocumento),
                new SqlParameter("@CodTipoDestinatario", this.CodTipoDestinatario)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteModelloStatoPraticaTipoDocumentoTipoDestinatario] " +
                "@IdModelloPratica, " +
                "@CodStatoPratica, " +
                "@CodTipoDocumento, " +
                "@CodTipoDestinatario", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdModelloPratica", this.IdModelloPratica),
                new SqlParameter("@CodStatoPratica", this.CodStatoPratica),
                new SqlParameter("@CodTipoDocumento", this.CodTipoDocumento),
                new SqlParameter("@CodTipoDestinatario", this.CodTipoDestinatario)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[InsertModelloStatoPraticaTipoDocumentoTipoDestinatario] " +
                "@IdModelloPratica, " +
                "@CodStatoPratica, " +
                "@CodTipoDocumento, " +
                "@CodTipoDestinatario", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
