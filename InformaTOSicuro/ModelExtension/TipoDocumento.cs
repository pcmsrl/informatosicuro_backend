﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class TipoDocumento
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Cod", (object)this.Cod)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteTipoDocumento] " +
                "@Cod ", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Cod", SqlDbType.VarChar, 50)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Descrizione", (object)this.Descrizione)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[InsertTipoDocumento] " +
                "@Cod OUTPUT, " +
                "@Descrizione ", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {

                this.Cod = (string)(parameterList.Where(x => x.ParameterName == "@Cod").FirstOrDefault()).Value;
            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Update(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Cod", (object)this.Cod),
                new SqlParameter("@Descrizione", (object)this.Descrizione ?? DBNull.Value)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpdateTipoDocumento] " +
                "@Cod, " +
                "@Descrizione ", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
