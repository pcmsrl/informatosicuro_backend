﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class RichiestaDocumentoPratica
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", (object)this.IdPratica),
                new SqlParameter("@NumRichiesta", (object)this.NumRichiesta)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteRichiestaDocumentoPratica] " +
                "@IdPratica, " +
                "@NumRichiesta ", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", (object)this.IdPratica),
                new SqlParameter("@NumRichiesta", SqlDbType.SmallInt)
                     { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@CodTipoDocumento", (object)this.CodTipoDocumento),
                new SqlParameter("@DataRichiesta", (object)this.DataRichiesta ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@UsernameRichiedente", (object)this.UsernameRichiedente ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@UsernameDestinatario", (object)this.UsernameDestinatario),
                new SqlParameter("@Descrizione", (object)this.Descrizione ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[InsertRichiestaDocumentoPratica] " +
                "@IdPratica, " +
                "@NumRichiesta OUTPUT, " +
                "@CodTipoDocumento, " +
                "@DataRichiesta, " +
                "@UsernameRichiedente, " +
                "@UsernameDestinatario, " +
                "@Descrizione", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {
                this.NumRichiesta = (short)(parameterList.Where(x => x.ParameterName == "@NumRichiesta").FirstOrDefault()).Value;
            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
