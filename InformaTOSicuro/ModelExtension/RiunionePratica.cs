﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class RiunionePratica
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdRiunione", (object)this.Id)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteRiunionePratica] " +
                "@IdRiunione", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdRiunione", SqlDbType.BigInt)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", (object)this.IdPratica),
                new SqlParameter("@DataRichiesta", (this.DataRichiesta == DateTime.MinValue ? DBNull.Value : (object)this.DataRichiesta) ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@DataRiunione", (object)this.DataRiunione ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Oggetto", (object)this.Oggetto ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Descrizione", (object)this.Descrizione ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Luogo", (object)this.Luogo ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@UsernameRichiedente", (object)this.UsernameRichiedente ?? DBNull.Value)
                    {IsNullable=true }
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertRiunionePratica] " +
                "@IdRiunione OUTPUT, " +
                "@IdPratica, " +
                "@DataRichiesta, " +
                "@DataRiunione, " +
                "@Oggetto, " +
                "@Descrizione, " +
                "@Luogo, " +
                "@UsernameRichiedente", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {
                this.Id = (long)(parameterList.Where(x => x.ParameterName == "@IdRiunione").FirstOrDefault()).Value;
            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Update(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdRiunione", (object)this.Id),
                new SqlParameter("@IdPratica", (object)this.IdPratica),
                new SqlParameter("@DataRichiesta", (this.DataRichiesta == DateTime.MinValue ? DBNull.Value : (object)this.DataRichiesta) ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@DataRiunione", (object)this.DataRiunione ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Oggetto", (object)this.Oggetto ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Descrizione", (object)this.Descrizione ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Luogo", (object)this.Luogo ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@UsernameRichiedente", (object)this.UsernameRichiedente ?? DBNull.Value)
                    {IsNullable=true }
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertRiunionePratica] " +
                "@IdRiunione OUTPUT, " +
                "@IdPratica, " +
                "@DataRichiesta, " +
                "@DataRiunione, " +
                "@Oggetto, " +
                "@Descrizione, " +
                "@Luogo, " +
                "@UsernameRichiedente", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
