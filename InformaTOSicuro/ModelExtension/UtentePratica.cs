﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class UtentePratica
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", this.IdPratica),
                new SqlParameter("@Username", (object)this.Username)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteUtentePratica] " +
                "@IdPratica, " +
                "@Username", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", this.IdPratica),
                new SqlParameter("@Username", (object)this.Username),
                new SqlParameter("@CodSpecializzazioneProfessionale", (object)this.CodSpecializzazioneProfessionale ?? DBNull.Value)
                    {IsNullable=true }
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertUtentePratica] " +
                "@IdPratica, " +
                "@Username, " +
                "@CodSpecializzazioneProfessionale", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Update(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", this.IdPratica),
                new SqlParameter("@Username", (object)this.Username),
                new SqlParameter("@CodSpecializzazioneProfessionale", (object)this.CodSpecializzazioneProfessionale ?? DBNull.Value)
                    {IsNullable=true }
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertUtentePratica] " +
                "@IdPratica, " +
                "@Username, " +
                "@CodSpecializzazioneProfessionale", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
