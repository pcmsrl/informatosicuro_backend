﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class Comunicazione
    {
        [NotMapped]
        public string EMail { get; set; }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<Microsoft.Data.SqlClient.SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Id", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@CodCanale",this.CodCanale),
                new SqlParameter("@CodModello",this.CodModello),
                new SqlParameter("@UsernameDestinatario",(object)this.UsernameDestinatario ?? DBNull.Value),
                new SqlParameter("@DataPrevistoInvio",(object)this.DataPrevistoInvio ?? DBNull.Value),
                new SqlParameter("@IdPratica",(object)this.IdPratica ?? DBNull.Value),
                new SqlParameter("@UID",(object)this.Uid ?? DBNull.Value),
                new SqlParameter("@NumRichiesta",(object)this.NumRichiesta ?? DBNull.Value),
                new SqlParameter("@IdRiunione",(object)this.IdRiunione ?? DBNull.Value),
                new SqlParameter("@NumDocumento",(object)this.NumDocumento ?? DBNull.Value)


            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[InsertComunicazione] " +
                "@Id = @Id, " +
                "@CodCanale = @CodCanale, " +
                "@CodModello = @CodModello, " +
                "@UsernameDestinatario = @UsernameDestinatario, " +
                "@DataPrevistoInvio = @DataPrevistoInvio, " +
                "@IdPratica = @IdPratica, " +
                "@UID = @UID, " +
                "@NumRichiesta = @NumRichiesta, " +
                "@IdRiunione = @IdRiunione, " +
                "@NumDocumento = @NumDocumento ", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
