﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class ListaPratica
    {
        [NotMapped]
        public List<EsitoStatoPratica> EsitoStatoPraticaAmmesso { get; set; }
    }
}
