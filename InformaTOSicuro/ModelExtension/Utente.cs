﻿using InformaTOSicuro.ModelExtension;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class Utente
    {
        [NotMapped]
        public string UncryptedPassword { get; set; }

        [NotMapped]
        public Guid? oneTimeToken { get; set; }

        public async Task<int> Insert(InformaTOSicuroContext context, bool? inseritoDaBakcOffice = null)
        {
            
            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Username", this.Username),
                new SqlParameter("@Password", (object)this.UncryptedPassword ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@Nome", (object)this.Nome ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@Cognome", (object)this.Cognome ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@DataNascita", (object)this.DataNascita ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CodComuneNascita", (object)this.CodComuneNascita ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CodiceFiscale", (object)this.CodiceFiscale ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@PartitaIVA", (object)this.PartitaIva ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@IndirizzoResidenza", (object)this.IndirizzoResidenza ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CivicoResidenza", (object)this.CivicoResidenza ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CAPResidenza", (object)this.Capresidenza ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CodComuneResidenza", (object)this.CodComuneResidenza ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@Telefono", (object)this.Telefono ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@EMail", (object)this.Email ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CodStatoUtente", (object)this.CodStato ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CodRuoloUtente", (object)this.CodRuolo ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@UidComunicazione", (object)this.oneTimeToken ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@InseritoDaBackOffice", (object)inseritoDaBakcOffice ?? DBNull.Value)
                { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[InsertUtente] " +
                "@Username, " + 
                "@Password, " + 
                "@Nome, " + 
                "@Cognome, " +
                "@DataNascita, " +
                "@CodComuneNascita, " +
                "@CodiceFiscale, " +
                "@PartitaIVA, " +
                "@IndirizzoResidenza, " +
                "@CivicoResidenza, " +
                "@CAPResidenza, " +
                "@CodComuneResidenza, " +
                "@Telefono, " +
                "@EMail, " +
                "@CodStatoUtente, " +
                "@CodRuoloUtente, " +
                "@UidComunicazione," +
                "@InseritoDaBackOffice", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }
        /*
        public async Task<List<ListaPratica>> ListaPratica(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@Username", this.Username)
            };

            return await context.ListaPratica.FromSqlRaw("SELECT * FROM [dbo].[ListaIdPratica_Utente](@Username) ", parameterList.ToArray()).ToListAsync();

        }*/

        public IQueryable<ListaPratica> ListaPratica(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@Username", this.Username)
            };

            return context.ListaPratica.FromSqlRaw("SELECT * FROM [dbo].[ListaIdPratica_Utente](@Username) ", parameterList.ToArray());

        }

        public async Task<int> SetPassword(InformaTOSicuroContext context, CambioPassword cambioPassword)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Username", this.Username),
                new SqlParameter("@OldPassword", (object)cambioPassword.OldPassword ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@NewPassword", cambioPassword.NewPassword),
                new SqlParameter("@UID", (object)cambioPassword.OneTimeToken ?? DBNull.Value)
                    { IsNullable=true}

            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[SetUtente_Password] " +
                "@Username, " +
                "@OldPassword, " +
                "@NewPassword, " +
                "@UID ", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> SetStato(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Username", this.Username),
                new SqlParameter("@CodStato", this.CodStato)

            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[SetUtente_Stato] " +
                "@Username, " +
                "@CodStato ", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public static ListaUtente Select(InformaTOSicuroContext context, byte[] accessToken,ref byte[] refreshToken)
        {
            /*
            byte[] byteToken = Enumerable.Range(0, accessToken.Length)
                                 .Where(x => x % 2 == 0)
                                 .Select(x => Convert.ToByte(accessToken.Substring(x, 2), 16))
                                 .ToArray();*/

                List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@AccessToken", accessToken),
                new SqlParameter("@Username", SqlDbType.VarChar, 50)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@RefreshToken", SqlDbType.Binary, 256)
                    { Direction = ParameterDirection.Output, IsNullable=true}
            };

            context.Database.ExecuteSqlRaw("EXECUTE @RC =  [dbo].[GetUtente_AccessToken] " +
                "@AccessToken, " +
                "@Username OUTPUT, " +
                "@RefreshToken OUTPUT ", parameterList.ToArray());

            int result = (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

            ListaUtente listaUtente = null;

            if (result == 0)
            {

                string username = (string)(parameterList.Where(x => x.ParameterName == "@Username").FirstOrDefault()).Value;
                
                if (!string.IsNullOrEmpty(parameterList.Where(x => x.ParameterName == "@RefreshToken").FirstOrDefault().Value.ToString()))
                {
                    refreshToken = (byte[])(parameterList.Where(x => x.ParameterName == "@RefreshToken").FirstOrDefault()).Value;
                }

                listaUtente = context.ListaUtente.Where(x => x.Username == username).FirstOrDefault();

            }

            return listaUtente;

        }

        public async static Task<ListaUtente> Select(InformaTOSicuroContext context, string username, string password)
        {
            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Username", username),
                new SqlParameter("@Password", password)
            };

            var listaUtente = await context.ListaUtente.FromSqlRaw("EXECUTE @RC =  [dbo].[GetUtente_Username_Password] " +
                "@Username, " +
                "@Password ", parameterList.ToArray()).ToListAsync();

            return listaUtente.FirstOrDefault();

        }

        public async Task<int> Update(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Username", this.Username),
                new SqlParameter("@Nome", (object)this.Nome ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@Cognome", (object)this.Cognome ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@DataNascita", (object)this.DataNascita ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CodComuneNascita", (object)this.CodComuneNascita ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CodiceFiscale", (object)this.CodiceFiscale ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@PartitaIVA", (object)this.PartitaIva ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@IndirizzoResidenza", (object)this.IndirizzoResidenza ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CivicoResidenza", (object)this.CivicoResidenza ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CAPResidenza", (object)this.Capresidenza ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CodComuneResidenza", (object)this.CodComuneResidenza ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@Telefono", (object)this.Telefono ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@EMail", (object)this.Email ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CodStatoUtente", (object)this.CodStato ?? DBNull.Value)
                { IsNullable=true},
                new SqlParameter("@CodRuoloUtente", (object)this.CodRuolo ?? DBNull.Value)
                { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpdateUtente] " +
                "@Username, " +
                "@Nome, " +
                "@Cognome, " +
                "@DataNascita, " +
                "@CodComuneNascita, " +
                "@CodiceFiscale, " +
                "@PartitaIVA, " +
                "@IndirizzoResidenza, " +
                "@CivicoResidenza, " +
                "@CAPResidenza, " +
                "@CodComuneResidenza, " +
                "@Telefono, " +
                "@EMail, " +
                "@CodStatoUtente, " +
                "@CodRuoloUtente", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
