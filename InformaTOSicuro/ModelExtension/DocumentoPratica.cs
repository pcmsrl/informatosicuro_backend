﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class DocumentoPratica
    {
        public string DocumentoBase64 { get; set; }

        private readonly string _path = "\\DocumentoPratica";

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", this.IdPratica),
                new SqlParameter("@NumDocumento", this.NumDocumento)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteDocumentoPratica] " +
                "@IdPratica, " +
                "@NumDocumento", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            // non db exception
            // 901 --> missing file
            // 902 --> missing tipo file

            if (string.IsNullOrEmpty(this.DocumentoBase64)) return (901);

            if (string.IsNullOrEmpty(this.TipoFile)) return (902);

            // il file type contiene il mime type. il nome file ha l'estensione completa
            //this.PercorsoFile = Path.Join(System.IO.Directory.GetCurrentDirectory(),_path,string.Concat(System.Guid.NewGuid(), ".", this.TipoFile));
            
            this.PercorsoFile = Path.Join(System.IO.Directory.GetCurrentDirectory(), _path, string.Concat(System.Guid.NewGuid(), Path.GetExtension( this.NomeFile)));

            File.WriteAllBytes(this.PercorsoFile,Convert.FromBase64String(this.DocumentoBase64));

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", this.IdPratica),
                new SqlParameter("@NumDocumento", SqlDbType.SmallInt)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@TipoFile", (object)this.TipoFile),
                new SqlParameter("@NomeFile", (object)this.NomeFile),
                new SqlParameter("@PercorsoFile", (object)this.PercorsoFile),
                new SqlParameter("@CodTipoDocumento", (object)this.CodTipoDocumento ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@DataInserimento", (object)this.DataInserimento ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@UsernameInserimento", (object)this.UsernameInserimento ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@NumRichiesta", (object)this.NumRichiesta ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@Note", (object)this.Note ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[InsertDocumentoPratica] " +
                "@IdPratica, " +
                "@NumDocumento OUTPUT, " +
                "@TipoFile, " +
                "@NomeFile, " +
                "@PercorsoFile, " +
                "@CodTipoDocumento, " +
                "@DataInserimento, " +
                "@UsernameInserimento, " +
                "@NumRichiesta, " +
                "@Note", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {

                this.NumDocumento = (short)(parameterList.Where(x => x.ParameterName == "@NumDocumento").FirstOrDefault()).Value;
            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
