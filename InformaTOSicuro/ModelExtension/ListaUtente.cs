﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace InformaTOSicuro.Model
{
    public partial class ListaUtente
    {
        [NotMapped]
        public List<ListaUtenteSpecializzazioneProfessionale> ListaUtenteSpecializzazioneProfessionale { get; set; }

    }
}
