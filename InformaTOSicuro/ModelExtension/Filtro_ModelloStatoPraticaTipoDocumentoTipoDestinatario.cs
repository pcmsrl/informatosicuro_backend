﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.ModelExtension
{
    public class Filtro_ModelloStatoPraticaTipoDocumentoTipoDestinatario
    {

        public string CodStatoPratica { get; set; }

        public string CodTipoDestinatario { get; set; }

    }
}
