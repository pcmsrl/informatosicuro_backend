﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class Pratica
    {

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Id", this.Id)
                     { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@UsernameRichiedente", (object)this.UsernameRichiedente),
                new SqlParameter("@IdAssociazione", (object)this.IdAssociazione),
                new SqlParameter("@IdModello", (object)this.IdModello ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@DataRichiesta", (object)this.DataRichiesta ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@Titolo", (object)this.Titolo ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@Descrizione", (object)this.Descrizione ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[InsertPratica] " +
                "@Id OUTPUT, " +
                "@UsernameRichiedente, " +
                "@IdAssociazione, " +
                "@IdModello, " +
                "@DataRichiesta, " +
                "@Titolo, " +
                "@Descrizione", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {

                this.Id = (long)(parameterList.Where(x => x.ParameterName == "@Id").FirstOrDefault()).Value;
            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> SetEsitoStato(InformaTOSicuroContext context, string codEsitoStato)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", (object)this.Id ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@CodEsito", (object)codEsitoStato ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@CodStatoSuccessivo", SqlDbType.Char,4)
                    { Direction = ParameterDirection.Output, IsNullable=true, SqlValue = DBNull.Value}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[SetPratica_EsitoStato] " +
                "@IdPratica, " +
                "@CodEsito, " +
                "@CodStatoSuccessivo OUTPUT ", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {

                this.CodStatoPratica = (string)(parameterList.Where(x => x.ParameterName == "@CodStatoSuccessivo").FirstOrDefault()).Value;
            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Update(InformaTOSicuroContext context, string usernameOperazione = null)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Id", this.Id),
                new SqlParameter("@Titolo", (object)this.Titolo ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@Descrizione", (object)this.Descrizione ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@UsernameResponsabile", (object)this.UsernameResponsabile ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@UsernameSegretaria", (object)this.UsernameSegretaria ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@UsernameOperazione", (object)usernameOperazione ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpdatePratica] " +
                "@Id, " +
                "@Titolo, " +
                "@Descrizione, " +
                "@UsernameResponsabile, " +
                "@UsernameSegretaria, " +
                "@UsernameOperazione", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
