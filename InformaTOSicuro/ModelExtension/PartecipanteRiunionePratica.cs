﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class PartecipanteRiunionePratica
    {


        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdRiunionePratica", (object)this.IdRiunionePratica),
                new SqlParameter("@UsernamePartecipante", (object)this.UsernamePartecipante)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeletePartecipanteRiunionePratica] " +
                "@IdRiunionePratica, " +
                "@UsernamePartecipante", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdRiunionePratica", (object)this.IdRiunionePratica),
                new SqlParameter("@UsernamePartcipante", (object)this.UsernamePartecipante),
                new SqlParameter("@DataInserimento", (this.DataInserimento == DateTime.MinValue ? DBNull.Value : (object)this.DataInserimento) ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Note", (object)this.Note ?? DBNull.Value)
                    {IsNullable=true }
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertPartecipanteRiunionePratica] " +
                "@IdRiunionePratica, " +
                "@UsernamePartcipante, " +
                "@DataInserimento, " +
                "@Note", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Update(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdRiunionePratica", (object)this.IdRiunionePratica),
                new SqlParameter("@UsernamePartcipante", (object)this.UsernamePartecipante),
                new SqlParameter("@DataInserimento", (this.DataInserimento == DateTime.MinValue ? DBNull.Value : (object)this.DataInserimento) ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Note", (object)this.Note ?? DBNull.Value)
                    {IsNullable=true }
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertPartecipanteRiunionePratica] " +
                "@IdRiunionePratica, " +
                "@UsernamePartcipante, " +
                "@DataInserimento, " +
                "@Note", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
