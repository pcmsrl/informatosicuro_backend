﻿using InformaTOSicuro.Model;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class EventoPratica
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", (object)this.IdPratica),
                new SqlParameter("@NumEvento", (object)this.NumEvento)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteEventoPratica] " +
                "@IdPratica, " +
                "@NumEvento ", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", (object)this.IdPratica),
                new SqlParameter("@NumEvento", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@CodTipoEvento", (object)this.CodTipoEvento),
                new SqlParameter("@Descrizione", (object)this.Descrizione ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@DataEvento", (this.DataEvento == DateTime.MinValue ? DBNull.Value : (object)this.DataEvento) ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@UsernameEvento", (object)this.UsernameEvento ?? DBNull.Value)
                    {IsNullable=true }
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[InsertEventoPratica] " +
                "@IdPratica, " +
                "@NumEvento OUTPUT, " +
                "@CodTipoEvento, " +
                "@Descrizione, " +
                "@DataEvento, " +
                "@UsernameEvento", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {
                this.NumEvento = (int)(parameterList.Where(x => x.ParameterName == "@NumEvento").FirstOrDefault()).Value;
            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Update(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", (object)this.IdPratica),
                new SqlParameter("@NumEvento", (object)this.NumEvento),
                new SqlParameter("@CodTipoEvento", (object)this.CodTipoEvento),
                new SqlParameter("@Descrizione", (object)this.Descrizione ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@DataEvento", (this.DataEvento == DateTime.MinValue ? DBNull.Value : (object)this.DataEvento) ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@UsernameEvento", (object)this.UsernameEvento ?? DBNull.Value)
                    {IsNullable=true }
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpdateEventoPratica] " +
                "@IdPratica, " +
                "@NumEvento, " +
                "@CodTipoEvento, " +
                "@Descrizione, " +
                "@DataEvento, " +
                "@UsernameEvento", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
