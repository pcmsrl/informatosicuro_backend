﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class MessaggioPratica
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdMessaggio", (object)this.Id)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteMessaggioPratica] " +
                "@IdMessaggio", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdMessaggio", SqlDbType.BigInt)
                     { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", (object)this.IdPratica),
                new SqlParameter("@Username", (object)this.Username),
                new SqlParameter("@Messaggio", (object)this.Messaggio ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@DataInserimento", (object)this.DataInserimento ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertMessaggioPratica] " +
                "@IdMessaggio OUTPUT, " +
                "@IdPratica, " +
                "@Username, " +
                "@Messaggio, " +
                "@DataInserimento", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {
                this.Id = (long)(parameterList.Where(x => x.ParameterName == "@IdMessaggio").FirstOrDefault()).Value;
            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Update(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdMessaggio", (object)this.Id),
                new SqlParameter("@IdPratica", (object)this.IdPratica),
                new SqlParameter("@Username", (object)this.Username),
                new SqlParameter("@Messaggio", (object)this.Messaggio ?? DBNull.Value)
                    { IsNullable=true},
                new SqlParameter("@DataInserimento", (object)this.DataInserimento ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertMessaggioPratica] " +
                "@IdMessaggio OUTPUT, " +
                "@IdPratica, " +
                "@Username, " +
                "@Messaggio, " +
                "@DataInserimento", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
