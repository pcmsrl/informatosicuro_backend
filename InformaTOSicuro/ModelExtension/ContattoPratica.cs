﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class ContattoPratica
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<Microsoft.Data.SqlClient.SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", this.IdPratica),
                new SqlParameter("@NumContatto", (object)this.NumContatto)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteContattoPratica] " +
                "@IdPratica, " +
                "@NumContatto", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", this.IdPratica),
                new SqlParameter("@NumContatto", (object)this.NumContatto ?? DBNull.Value)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@EMail", (object)this.Email ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Telefono", (object)this.Telefono ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Cognome", (object)this.Cognome ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Nome", (object)this.Nome ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@CodSpecializzazioneProfessionale", (object)this.CodSpecializzazioneProfessionale ?? DBNull.Value)
                    {IsNullable=true }
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertContattoPratica] " +
                "@IdPratica, " +
                "@NumContatto OUTPUT, " +
                "@EMail, " +
                "@Telefono, " +
                "@Cognome, " +
                "@Nome, " +
                "@CodSpecializzazioneProfessionale", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {
                this.NumContatto = (short)(parameterList.Where(x => x.ParameterName == "@NumContatto").FirstOrDefault()).Value;
            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Update(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdPratica", this.IdPratica),
                new SqlParameter("@NumContatto", this.NumContatto),
                new SqlParameter("@EMail", (object)this.Email ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Telefono", (object)this.Telefono ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Cognome", (object)this.Cognome ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@Nome", (object)this.Nome ?? DBNull.Value)
                    {IsNullable=true },
                new SqlParameter("@CodSpecializzazioneProfessionale", (object)this.CodSpecializzazioneProfessionale ?? DBNull.Value)
                    {IsNullable=true }
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertContattoPratica] " +
                "@IdPratica, " +
                "@NumContatto OUTPUT, " +
                "@EMail, " +
                "@Telefono, " +
                "@Cognome, " +
                "@Nome, " +
                "@CodSpecializzazioneProfessionale", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
