﻿using InformaTOSicuro.Model;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class AccessToken
    {
        [NotMapped]
        public virtual ListaUtente ListaUtente { get; set; }

        public async Task<int> Insert(InformaTOSicuroContext context, ListaUtente utente)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Id", SqlDbType.BigInt)
                     { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@AccessToken", SqlDbType.Binary, 256)
                     { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@DataCreazione", (object)this.DataCreazione ?? DBNull.Value),
                new SqlParameter("@Username", (object)utente.Username)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[InsertAccessToken] " +
                "@Id OUTPUT, " +
                "@AccessToken OUTPUT, " +
                "@DataCreazione, " +
                "@Username", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {

                this.Id = (long)(parameterList.Where(x => x.ParameterName == "@Id").FirstOrDefault()).Value;
                this.AccessToken1 = (byte[])(parameterList.Where(x => x.ParameterName == "@AccessToken").FirstOrDefault()).Value;
                this.Username = utente.Username;
                this.ListaUtente = utente;

            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
