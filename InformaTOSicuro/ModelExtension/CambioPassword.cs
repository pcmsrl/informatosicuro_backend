﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.ModelExtension
{
    public class CambioPassword
    {

        public string Username { get; set; }

        public Guid? OneTimeToken { get; set; }

        public string OldPassword { get; set; }

        public string NewPassword { get; set; }

    }
}
