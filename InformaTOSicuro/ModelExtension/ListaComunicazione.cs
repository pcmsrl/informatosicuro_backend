﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class ListaComunicazione
    {


        public async Task<int> SetDataInvio(InformaTOSicuroContext context)
        {

            List<Microsoft.Data.SqlClient.SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdComunicazione", this.Id),
                new SqlParameter("@DataInvio", (object)this.DataInvio)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[SetComunicazione_DataInvio] " +
                "@IdComunicazione, " +
                "@DataInvio", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
