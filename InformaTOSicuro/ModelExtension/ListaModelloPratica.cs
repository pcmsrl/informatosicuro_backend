﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class ListaModelloPratica
    {

        [NotMapped]
        public List<ListaModelloPraticaSpecializzazioneProfessionale> ListaModelloPraticaSpecializzazioneProfessionale { get; set; }

        [NotMapped]
        public List<ListaModelloStatoPraticaTipoDocumentoTipoDestinatario> ListaModelloStatoPraticaTipoDocumentoTipoDestinatario { get; set; }

    }
}
