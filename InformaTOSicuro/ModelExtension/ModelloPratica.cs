﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class ModelloPratica
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Id", this.Id),
                new SqlParameter("@DataCancellazione", (this.DataCancellazione == DateTime.MinValue ? DBNull.Value : (object)this.DataCancellazione) ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteModelloPratica] " +
                "@Id, " +
                "@DataCancellazione", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Id", this.Id)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdAssociazione", (object)this.IdAssociazione),
                new SqlParameter("@Titolo", (object)this.Titolo),
                new SqlParameter("@Descrizione", (object)this.Descrizione),
                new SqlParameter("@UsernameCreazione", (object)this.UsernameCreazione),
                new SqlParameter("@DataCreazione", (this.DataCreazione == DateTime.MinValue ? DBNull.Value : (object)this.DataCreazione) ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertModelloPratica] " +
                "@Id OUTPUT, " +
                "@IdAssociazione, " +
                "@Titolo, " +
                "@Descrizione, " +
                "@UsernameCreazione, " +
                "@DataCreazione", parameterList.ToArray());

            if ((int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value == 0)
            {

                this.Id = (int)(parameterList.Where(x => x.ParameterName == "@Id").FirstOrDefault()).Value;
            }

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Update(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Id", this.Id),
                new SqlParameter("@IdAssociazione", (object)this.IdAssociazione),
                new SqlParameter("@Titolo", (object)this.Titolo),
                new SqlParameter("@Descrizione", (object)this.Descrizione),
                new SqlParameter("@UsernameCreazione", (object)this.UsernameCreazione ?? DBNull.Value)
                { IsNullable = true},
                new SqlParameter("@DataCreazione", (this.DataCreazione == DateTime.MinValue ? DBNull.Value : (object)this.DataCreazione) ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertModelloPratica] " +
                "@Id OUTPUT, " +
                "@IdAssociazione, " +
                "@Titolo, " +
                "@Descrizione, " +
                "@UsernameCreazione, " +
                "@DataCreazione", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
