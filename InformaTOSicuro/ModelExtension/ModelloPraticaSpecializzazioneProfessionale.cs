﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class ModelloPraticaSpecializzazioneProfessionale
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdModelloPratica", this.IdModelloPratica),
                new SqlParameter("@CodSpecializzazioneProfessionale", this.CodSpecializzazioneProfessionale)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteModelloPraticaSpecializzazioneProfessionale] " +
                "@IdModelloPratica, " +
                "@CodSpecializzazioneProfessionale", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdModelloPratica", this.IdModelloPratica),
                new SqlParameter("@CodSpecializzazioneProfessionale", this.CodSpecializzazioneProfessionale),
                new SqlParameter("@Quantita", (object)this.Quantita ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertModelloPraticaSpecializzazioneProfessionale] " +
                "@IdModelloPratica, " +
                "@CodSpecializzazioneProfessionale, " +
                "@Quantita", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Update(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@IdModelloPratica", this.IdModelloPratica),
                new SqlParameter("@CodSpecializzazioneProfessionale", this.CodSpecializzazioneProfessionale),
                new SqlParameter("@Quantita", (object)this.Quantita ?? DBNull.Value)
                    { IsNullable=true}
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[UpsertModelloPraticaSpecializzazioneProfessionale] " +
                "@IdModelloPratica, " +
                "@CodSpecializzazioneProfessionale, " +
                "@Quantita", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
