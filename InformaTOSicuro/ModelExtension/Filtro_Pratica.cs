﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.ModelExtension
{
    public class Filtro_Pratica
    {

        public string CodStatoPratica { get; set; }

        public Boolean SoloChiusa { get; set; }

        public Boolean SoloAperta { get; set; }

        public string Richiedente { get; set; }

        public DateTime DataRichiestaDa { get; set; }

        public DateTime DataRichiestaA { get; set; }

        public string UsernameResponsabile { get; set; }

        public string UsernameSegretaria { get; set; }

    }
}
