﻿using InformaTOSicuro.Model;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InformaTOSicuro.Model
{
    public partial class UtenteSpecializzazioneProfessionale
    {

        public async Task<int> Delete(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Username", (object)this.Username),
                new SqlParameter("@CodSpecializzazioneProfessionale", this.CodSpecializzazioneProfessionale)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[DeleteUtenteSpecializzazioneProfessionale] " +
                "@Username, " +
                "@CodSpecializzazioneProfessionale", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

        public async Task<int> Insert(InformaTOSicuroContext context)
        {

            List<SqlParameter> parameterList = new List<SqlParameter>()
            {
                new SqlParameter("@RC", SqlDbType.Int)
                    { Direction = ParameterDirection.Output, IsNullable=true},
                new SqlParameter("@Username", (object)this.Username),
                new SqlParameter("@CodSpecializzazioneProfessionale", this.CodSpecializzazioneProfessionale)
            };

            await context.Database.ExecuteSqlRawAsync("EXECUTE @RC = [dbo].[InsertUtenteSpecializzazioneProfessionale] " +
                "@Username, " +
                "@CodSpecializzazioneProfessionale", parameterList.ToArray());

            return (int)(parameterList.Where(x => x.ParameterName == "@RC").FirstOrDefault()).Value;

        }

    }
}
