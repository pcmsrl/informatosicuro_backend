﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaUtenteSpecializzazioneProfessionale
    {
        public string Username { get; set; }
        public string NomeUtente { get; set; }
        public string CognomeUtente { get; set; }
        public string CodSpecializzazioneProfessionale { get; set; }
        public string SpecializzazioneProfessionale { get; set; }
    }
}
