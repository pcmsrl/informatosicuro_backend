﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class MessaggioPratica
    {
        public long Id { get; set; }
        public long IdPratica { get; set; }
        public string Username { get; set; }
        public string Messaggio { get; set; }
        public DateTime? DataInserimento { get; set; }
        public DateTime? DataCancellazione { get; set; }

        public virtual Pratica IdPraticaNavigation { get; set; }
        public virtual Utente UsernameNavigation { get; set; }
    }
}
