﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class TipoDestinatario
    {
        public TipoDestinatario()
        {
            ModelloStatoPraticaTipoDocumentoTipoDestinatario = new HashSet<ModelloStatoPraticaTipoDocumentoTipoDestinatario>();
        }

        public string Cod { get; set; }
        public string Descrizione { get; set; }

        public virtual ICollection<ModelloStatoPraticaTipoDocumentoTipoDestinatario> ModelloStatoPraticaTipoDocumentoTipoDestinatario { get; set; }
    }
}
