﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class Provincia
    {
        public Provincia()
        {
            Comune = new HashSet<Comune>();
        }

        public string Sigla { get; set; }
        public string Provincia1 { get; set; }
        public string Regione { get; set; }

        public virtual ICollection<Comune> Comune { get; set; }
    }
}
