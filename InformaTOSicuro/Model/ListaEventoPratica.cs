﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaEventoPratica
    {
        public long IdPratica { get; set; }
        public string Pratica { get; set; }
        public int? NumEvento { get; set; }
        public string CodTipoEvento { get; set; }
        public string TipoEvento { get; set; }
        public string Descrizione { get; set; }
        public DateTime? DataEvento { get; set; }
        public string UsernameEvento { get; set; }
        public string CognomeUtenteEvento { get; set; }
        public string NomeUtenteEvento { get; set; }
        public long? IdRiunione { get; set; }
    }
}
