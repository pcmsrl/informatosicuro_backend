﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class Comune
    {
        public Comune()
        {
            Associazione = new HashSet<Associazione>();
            UtenteCodComuneNascitaNavigation = new HashSet<Utente>();
            UtenteCodComuneResidenzaNavigation = new HashSet<Utente>();
        }

        public string Cod { get; set; }
        public string Comune1 { get; set; }
        public string CodCatastale { get; set; }
        public string SiglaProvincia { get; set; }

        public virtual Provincia SiglaProvinciaNavigation { get; set; }
        public virtual ICollection<Associazione> Associazione { get; set; }
        public virtual ICollection<Utente> UtenteCodComuneNascitaNavigation { get; set; }
        public virtual ICollection<Utente> UtenteCodComuneResidenzaNavigation { get; set; }
    }
}
