﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ModelloComunicazione
    {
        public ModelloComunicazione()
        {
            Comunicazione = new HashSet<Comunicazione>();
        }

        public string Cod { get; set; }
        public string Descrizione { get; set; }

        public virtual ICollection<Comunicazione> Comunicazione { get; set; }
    }
}
