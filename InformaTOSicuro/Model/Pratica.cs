﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class Pratica
    {
        public Pratica()
        {
            ContattoPratica = new HashSet<ContattoPratica>();
            DocumentoPratica = new HashSet<DocumentoPratica>();
            EventoPratica = new HashSet<EventoPratica>();
            MessaggioPratica = new HashSet<MessaggioPratica>();
            RichiestaDocumentoPratica = new HashSet<RichiestaDocumentoPratica>();
        }

        public long Id { get; set; }
        public string UsernameRichiedente { get; set; }
        public int IdAssociazione { get; set; }
        public int? IdModello { get; set; }
        public DateTime DataRichiesta { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public string CodStatoPratica { get; set; }
        public string UsernameResponsabile { get; set; }
        public DateTime? DataCancellazione { get; set; }
        public string UsernameSegretaria { get; set; }
        public string CodStatoPraticaPrecedente { get; set; }
        public string CodEsitoStatoPraticaPrecedente { get; set; }

        public virtual StatoPratica CodStatoPraticaNavigation { get; set; }
        public virtual Associazione IdAssociazioneNavigation { get; set; }
        public virtual ModelloPratica IdModelloNavigation { get; set; }
        public virtual Utente UsernameResponsabileNavigation { get; set; }
        public virtual Utente UsernameRichiedenteNavigation { get; set; }
        public virtual ICollection<ContattoPratica> ContattoPratica { get; set; }
        public virtual ICollection<DocumentoPratica> DocumentoPratica { get; set; }
        public virtual ICollection<EventoPratica> EventoPratica { get; set; }
        public virtual ICollection<MessaggioPratica> MessaggioPratica { get; set; }
        public virtual ICollection<RichiestaDocumentoPratica> RichiestaDocumentoPratica { get; set; }
    }
}
