﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class Associazione
    {
        public Associazione()
        {
            ModelloPratica = new HashSet<ModelloPratica>();
            Pratica = new HashSet<Pratica>();
            UtenteAssociazione = new HashSet<UtenteAssociazione>();
        }

        public int Id { get; set; }
        public string RagioneSociale { get; set; }
        public string Indirizzo { get; set; }
        public string Cap { get; set; }
        public string CodComune { get; set; }
        public string PartitaIva { get; set; }
        public string CodiceFiscale { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }

        public virtual Comune CodComuneNavigation { get; set; }
        public virtual ICollection<ModelloPratica> ModelloPratica { get; set; }
        public virtual ICollection<Pratica> Pratica { get; set; }
        public virtual ICollection<UtenteAssociazione> UtenteAssociazione { get; set; }
    }
}
