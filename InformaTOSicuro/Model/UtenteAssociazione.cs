﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class UtenteAssociazione
    {
        public string Username { get; set; }
        public int IdAssociazione { get; set; }
        public DateTime? DataInizioRapporto { get; set; }
        public DateTime? DataFineRapporto { get; set; }
        public string Urlcv { get; set; }

        public virtual Associazione IdAssociazioneNavigation { get; set; }
        public virtual Utente UsernameNavigation { get; set; }
    }
}
