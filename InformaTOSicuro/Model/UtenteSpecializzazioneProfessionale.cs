﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class UtenteSpecializzazioneProfessionale
    {
        public string Username { get; set; }
        public string CodSpecializzazioneProfessionale { get; set; }
    }
}
