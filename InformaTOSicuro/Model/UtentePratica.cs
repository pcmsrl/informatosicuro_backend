﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class UtentePratica
    {
        public long IdPratica { get; set; }
        public string Username { get; set; }
        public string CodSpecializzazioneProfessionale { get; set; }
    }
}
