﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaComunicazione
    {
        public long Id { get; set; }
        public string CodCanale { get; set; }
        public string Canale { get; set; }
        public string CodModello { get; set; }
        public string Modello { get; set; }
        public string UsernameDestinatario { get; set; }
        public string NomeDestinatario { get; set; }
        public string CognomeDestinatario { get; set; }
        public string EmailDestinatario { get; set; }
        public DateTime? DataPrevistoInvio { get; set; }
        public DateTime? DataInvio { get; set; }
        public long? IdPratica { get; set; }
        public string TitoloPratica { get; set; }
        public Guid? Uid { get; set; }
        public short? NumRichiesta { get; set; }
        public string Richiesta { get; set; }
        public long? IdRiunione { get; set; }
        public string Riunione { get; set; }
        public DateTime? DataRiunione { get; set; }
        public short? NumDocumento { get; set; }
        public string Documento { get; set; }
        public short? NumContatto { get; set; }
        public string NomeContatto { get; set; }
        public string CognomeContatto { get; set; }
        public string EmailContatto { get; set; }
        public string CodRuolo { get; set; }
    }
}
