﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaUtenteAssociazione
    {
        public string UsernameProfessionista { get; set; }
        public string CognomeProfessionista { get; set; }
        public string NomeProfessionista { get; set; }
        public int IdAssociazione { get; set; }
        public string Associazione { get; set; }
        public DateTime? DataInizioRapporto { get; set; }
        public DateTime? DataFineRapporto { get; set; }
        public string Urlcv { get; set; }
    }
}
