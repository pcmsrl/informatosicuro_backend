﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ModelloPratica
    {
        public ModelloPratica()
        {
            ModelloStatoPraticaTipoDocumento = new HashSet<ModelloStatoPraticaTipoDocumento>();
            Pratica = new HashSet<Pratica>();
        }

        public int Id { get; set; }
        public int IdAssociazione { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public string UsernameCreazione { get; set; }
        public DateTime DataCreazione { get; set; }
        public DateTime? DataCancellazione { get; set; }

        public virtual Associazione IdAssociazioneNavigation { get; set; }
        public virtual ICollection<ModelloStatoPraticaTipoDocumento> ModelloStatoPraticaTipoDocumento { get; set; }
        public virtual ICollection<Pratica> Pratica { get; set; }
    }
}
