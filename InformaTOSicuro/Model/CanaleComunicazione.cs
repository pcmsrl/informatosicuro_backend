﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class CanaleComunicazione
    {
        public CanaleComunicazione()
        {
            Comunicazione = new HashSet<Comunicazione>();
        }

        public string Cod { get; set; }
        public string Descrizione { get; set; }

        public virtual ICollection<Comunicazione> Comunicazione { get; set; }
    }
}
