﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ModelloStatoPraticaTipoDocumentoTipoDestinatario
    {
        public int IdModelloPratica { get; set; }
        public string CodStatoPratica { get; set; }
        public string CodTipoDocumento { get; set; }
        public string CodTipoDestinatario { get; set; }

        public virtual TipoDestinatario CodTipoDestinatarioNavigation { get; set; }
        public virtual ModelloStatoPraticaTipoDocumento ModelloStatoPraticaTipoDocumento { get; set; }
    }
}
