﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaDocumentoPratica
    {
        public long IdPratica { get; set; }
        public string Titolo { get; set; }
        public short NumDocumento { get; set; }
        public string TipoFile { get; set; }
        public string NomeFile { get; set; }
        public string PercorsoFile { get; set; }
        public string CodTipoDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public DateTime? DataInserimento { get; set; }
        public string UsernameInserimento { get; set; }
        public string CognomeUtenteInserimento { get; set; }
        public string NomeUtenteInserimento { get; set; }
        public short? NumRichiesta { get; set; }
        public string DescrizioneRichiesta { get; set; }
        public string Note { get; set; }
    }
}
