﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class RichiestaDocumentoPratica
    {
        public RichiestaDocumentoPratica()
        {
            DocumentoPratica = new HashSet<DocumentoPratica>();
        }

        public long IdPratica { get; set; }
        public short NumRichiesta { get; set; }
        public string CodTipoDocumento { get; set; }
        public DateTime DataRichiesta { get; set; }
        public string UsernameRichiedente { get; set; }
        public string UsernameDestinatario { get; set; }
        public string Descrizione { get; set; }
        public DateTime? DataCancellazione { get; set; }

        public virtual TipoDocumento CodTipoDocumentoNavigation { get; set; }
        public virtual Pratica IdPraticaNavigation { get; set; }
        public virtual Utente UsernameDestinatarioNavigation { get; set; }
        public virtual Utente UsernameRichiedenteNavigation { get; set; }
        public virtual ICollection<DocumentoPratica> DocumentoPratica { get; set; }
    }
}
