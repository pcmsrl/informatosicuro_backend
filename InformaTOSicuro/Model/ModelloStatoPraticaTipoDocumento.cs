﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ModelloStatoPraticaTipoDocumento
    {
        public ModelloStatoPraticaTipoDocumento()
        {
            ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale = new HashSet<ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale>();
            ModelloStatoPraticaTipoDocumentoTipoDestinatario = new HashSet<ModelloStatoPraticaTipoDocumentoTipoDestinatario>();
        }

        public int IdModelloPratica { get; set; }
        public string CodStatoPratica { get; set; }
        public string CodTipoDocumento { get; set; }

        public virtual StatoPratica CodStatoPraticaNavigation { get; set; }
        public virtual TipoDocumento CodTipoDocumentoNavigation { get; set; }
        public virtual ModelloPratica IdModelloPraticaNavigation { get; set; }
        public virtual ICollection<ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale> ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale { get; set; }
        public virtual ICollection<ModelloStatoPraticaTipoDocumentoTipoDestinatario> ModelloStatoPraticaTipoDocumentoTipoDestinatario { get; set; }
    }
}
