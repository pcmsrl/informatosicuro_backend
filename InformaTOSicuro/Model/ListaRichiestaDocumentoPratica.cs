﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaRichiestaDocumentoPratica
    {
        public long IdPratica { get; set; }
        public short NumRichiesta { get; set; }
        public string CodTipoDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public DateTime DataRichiesta { get; set; }
        public string UsernameRichiedente { get; set; }
        public string CognomeRichiedente { get; set; }
        public string NomeRichiedente { get; set; }
        public string UsernameDestinatario { get; set; }
        public string CognomeDestinatario { get; set; }
        public string NomeDestinatario { get; set; }
        public string Descrizione { get; set; }
        public int DocumentoFornito { get; set; }
    }
}
