﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class EventoPratica
    {
        public long IdPratica { get; set; }
        public int NumEvento { get; set; }
        public string CodTipoEvento { get; set; }
        public string Descrizione { get; set; }
        public DateTime? DataEvento { get; set; }
        public string UsernameEvento { get; set; }
        public short? NumDocumento { get; set; }

        public virtual TipoEventoPratica CodTipoEventoNavigation { get; set; }
        public virtual DocumentoPratica DocumentoPratica { get; set; }
        public virtual Pratica IdPraticaNavigation { get; set; }
    }
}
