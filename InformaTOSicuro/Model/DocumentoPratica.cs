﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class DocumentoPratica
    {
        public DocumentoPratica()
        {
            EventoPratica = new HashSet<EventoPratica>();
        }

        public long IdPratica { get; set; }
        public short NumDocumento { get; set; }
        public string TipoFile { get; set; }
        public string NomeFile { get; set; }
        public string PercorsoFile { get; set; }
        public string CodTipoDocumento { get; set; }
        public DateTime? DataInserimento { get; set; }
        public DateTime? DataCancellazione { get; set; }
        public string UsernameInserimento { get; set; }
        public short? NumRichiesta { get; set; }
        public string Note { get; set; }

        public virtual Pratica IdPraticaNavigation { get; set; }
        public virtual RichiestaDocumentoPratica RichiestaDocumentoPratica { get; set; }
        public virtual Utente UsernameInserimentoNavigation { get; set; }
        public virtual ICollection<EventoPratica> EventoPratica { get; set; }
    }
}
