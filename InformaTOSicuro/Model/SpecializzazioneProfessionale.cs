﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class SpecializzazioneProfessionale
    {
        public string Cod { get; set; }
        public string Descrizione { get; set; }
        public DateTime? DataCancellazione { get; set; }
    }
}
