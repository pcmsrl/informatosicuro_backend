﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class TipoEventoPratica
    {
        public TipoEventoPratica()
        {
            EventoPratica = new HashSet<EventoPratica>();
            StatoPratica = new HashSet<StatoPratica>();
        }

        public string Cod { get; set; }
        public string Descrizione { get; set; }
        public bool? SelezionabileUtente { get; set; }

        public virtual ICollection<EventoPratica> EventoPratica { get; set; }
        public virtual ICollection<StatoPratica> StatoPratica { get; set; }
    }
}
