﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class EsitoStatoPratica
    {
        public string CodStato { get; set; }
        public string CodEsito { get; set; }
        public string Descrizione { get; set; }
        public string CodStatoSuccessivo { get; set; }
        public bool? RichiestoResponsabile { get; set; }
        public string TestoConferma { get; set; }

        public virtual StatoPratica CodStatoNavigation { get; set; }
        public virtual StatoPratica CodStatoSuccessivoNavigation { get; set; }
    }
}
