﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaModelloPraticaSpecializzazioneProfessionale
    {
        public int IdModelloPratica { get; set; }
        public string ModelloPratica { get; set; }
        public string CodSpecializzazioneProfessionale { get; set; }
        public string SpecializzazioneProfessionale { get; set; }
        public short Quantita { get; set; }
    }
}
