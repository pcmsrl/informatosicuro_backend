﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaContattoPratica
    {
        public long IdPratica { get; set; }
        public string UsernameRichiedente { get; set; }
        public int IdAssociazione { get; set; }
        public string Titolo { get; set; }
        public short NumContatto { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Cognome { get; set; }
        public string Nome { get; set; }
        public string CodSpecializzazioneProfessionale { get; set; }
        public string SpecializzazioneProfessionale { get; set; }
    }
}
