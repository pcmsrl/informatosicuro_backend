﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale
    {
        public int IdModelloPratica { get; set; }
        public string CodStatoPratica { get; set; }
        public string CodTipoDocumento { get; set; }
        public string CodSpecializzazioneProfessionale { get; set; }

        public virtual ModelloPraticaSpecializzazioneProfessionale ModelloPraticaSpecializzazioneProfessionale { get; set; }
        public virtual ModelloStatoPraticaTipoDocumento ModelloStatoPraticaTipoDocumento { get; set; }
    }
}
