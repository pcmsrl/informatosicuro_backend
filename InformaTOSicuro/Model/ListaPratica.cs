﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaPratica
    {
        public long Id { get; set; }
        public string UsernameRichiedente { get; set; }
        public string NomeRichiedente { get; set; }
        public string CognomeRichiedente { get; set; }
        public string Richiedente { get; set; }
        public string SiglaProvinciaResidenzaRichiedente { get; set; }
        public string ComuneResidenzaRichiedente { get; set; }
        public string EmailRichiedente { get; set; }
        public string TelefonoRichiedente { get; set; }
        public int IdAssociazione { get; set; }
        public string Associazione { get; set; }
        public int? IdModello { get; set; }
        public string ModelloPratica { get; set; }
        public DateTime DataRichiesta { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public string CodStatoPratica { get; set; }
        public string StatoPratica { get; set; }
        public string UsernameResponsabile { get; set; }
        public string NomeResponsabile { get; set; }
        public string CognomeResponsabile { get; set; }
        public string UsernameSegretaria { get; set; }
        public string NomeSegretaria { get; set; }
        public string CognomeSegretaria { get; set; }
        public string CodEsitoStatoPraticaPrecedente { get; set; }
        public string CodStatoPraticaPrecedente { get; set; }
        public string EsitoStatoPraticaPrecedente { get; set; }
    }
}
