﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class StatoPratica
    {
        public StatoPratica()
        {
            EsitoStatoPraticaCodStatoNavigation = new HashSet<EsitoStatoPratica>();
            EsitoStatoPraticaCodStatoSuccessivoNavigation = new HashSet<EsitoStatoPratica>();
            ModelloStatoPraticaTipoDocumento = new HashSet<ModelloStatoPraticaTipoDocumento>();
            Pratica = new HashSet<Pratica>();
        }

        public string Cod { get; set; }
        public string Descrizione { get; set; }
        public bool? Inizio { get; set; }
        public string CodTipoEvento { get; set; }

        public virtual TipoEventoPratica CodTipoEventoNavigation { get; set; }
        public virtual ICollection<EsitoStatoPratica> EsitoStatoPraticaCodStatoNavigation { get; set; }
        public virtual ICollection<EsitoStatoPratica> EsitoStatoPraticaCodStatoSuccessivoNavigation { get; set; }
        public virtual ICollection<ModelloStatoPraticaTipoDocumento> ModelloStatoPraticaTipoDocumento { get; set; }
        public virtual ICollection<Pratica> Pratica { get; set; }
    }
}
