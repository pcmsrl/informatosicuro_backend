﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaModelloStatoPraticaTipoDocumento
    {
        public int IdModelloPratica { get; set; }
        public string ModelloPratica { get; set; }
        public string CodStatoPratica { get; set; }
        public string StatoPratica { get; set; }
        public string CodTipoDocumento { get; set; }
        public string TipoDocumento { get; set; }
    }
}
