﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaUtente
    {
        public string Username { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public DateTime? DataNascita { get; set; }
        public string CodComuneNascita { get; set; }
        public string ComuneNascita { get; set; }
        public string SiglaProvinciaNascita { get; set; }
        public string CodiceFiscale { get; set; }
        public string PartitaIva { get; set; }
        public string IndirizzoResidenza { get; set; }
        public string CivicoResidenza { get; set; }
        public string Capresidenza { get; set; }
        public string CodComuneResidenza { get; set; }
        public string ComuneResidenza { get; set; }
        public string SiglaprovinciaResidenza { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string CodStato { get; set; }
        public string StatoUtente { get; set; }
        public string CodRuolo { get; set; }
        public string RuoloUtente { get; set; }
        public string UrlCv { get; set; }
        public int? PraticheRichieste { get; set; }
        public int? PraticheCollaborazione { get; set; }
    }
}
