﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ModelloPraticaSpecializzazioneProfessionale
    {
        public ModelloPraticaSpecializzazioneProfessionale()
        {
            ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale = new HashSet<ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale>();
        }

        public int IdModelloPratica { get; set; }
        public string CodSpecializzazioneProfessionale { get; set; }
        public short Quantita { get; set; }

        public virtual ICollection<ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale> ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale { get; set; }
    }
}
