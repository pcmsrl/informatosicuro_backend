﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class TipoDocumento
    {
        public TipoDocumento()
        {
            ModelloStatoPraticaTipoDocumento = new HashSet<ModelloStatoPraticaTipoDocumento>();
            RichiestaDocumentoPratica = new HashSet<RichiestaDocumentoPratica>();
        }

        public string Cod { get; set; }
        public string Descrizione { get; set; }
        public DateTime? DataCancellazione { get; set; }

        public virtual ICollection<ModelloStatoPraticaTipoDocumento> ModelloStatoPraticaTipoDocumento { get; set; }
        public virtual ICollection<RichiestaDocumentoPratica> RichiestaDocumentoPratica { get; set; }
    }
}
