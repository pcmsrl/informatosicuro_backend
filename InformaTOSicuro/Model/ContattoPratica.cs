﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ContattoPratica
    {
        public long IdPratica { get; set; }
        public short NumContatto { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Cognome { get; set; }
        public string Nome { get; set; }
        public string CodSpecializzazioneProfessionale { get; set; }
        public string Username { get; set; }
        public DateTime? DataUsername { get; set; }

        public virtual Pratica IdPraticaNavigation { get; set; }
    }
}
