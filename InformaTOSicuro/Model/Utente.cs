﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class Utente
    {
        public Utente()
        {
            AccessToken = new HashSet<AccessToken>();
            Comunicazione = new HashSet<Comunicazione>();
            DocumentoPratica = new HashSet<DocumentoPratica>();
            MessaggioPratica = new HashSet<MessaggioPratica>();
            PartecipanteRiunionePratica = new HashSet<PartecipanteRiunionePratica>();
            PraticaUsernameResponsabileNavigation = new HashSet<Pratica>();
            PraticaUsernameRichiedenteNavigation = new HashSet<Pratica>();
            RichiestaDocumentoPraticaUsernameDestinatarioNavigation = new HashSet<RichiestaDocumentoPratica>();
            RichiestaDocumentoPraticaUsernameRichiedenteNavigation = new HashSet<RichiestaDocumentoPratica>();
            UtenteAssociazione = new HashSet<UtenteAssociazione>();
        }

        public string Username { get; set; }
        public byte[] Password { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public DateTime? DataNascita { get; set; }
        public string CodComuneNascita { get; set; }
        public string CodiceFiscale { get; set; }
        public string PartitaIva { get; set; }
        public string IndirizzoResidenza { get; set; }
        public string Capresidenza { get; set; }
        public string CodComuneResidenza { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string CodStato { get; set; }
        public string CodRuolo { get; set; }
        public string CivicoResidenza { get; set; }
        public string UrlCv { get; set; }

        public virtual Comune CodComuneNascitaNavigation { get; set; }
        public virtual Comune CodComuneResidenzaNavigation { get; set; }
        public virtual RuoloUtente CodRuoloNavigation { get; set; }
        public virtual StatoUtente CodStatoNavigation { get; set; }
        public virtual ICollection<AccessToken> AccessToken { get; set; }
        public virtual ICollection<Comunicazione> Comunicazione { get; set; }
        public virtual ICollection<DocumentoPratica> DocumentoPratica { get; set; }
        public virtual ICollection<MessaggioPratica> MessaggioPratica { get; set; }
        public virtual ICollection<PartecipanteRiunionePratica> PartecipanteRiunionePratica { get; set; }
        public virtual ICollection<Pratica> PraticaUsernameResponsabileNavigation { get; set; }
        public virtual ICollection<Pratica> PraticaUsernameRichiedenteNavigation { get; set; }
        public virtual ICollection<RichiestaDocumentoPratica> RichiestaDocumentoPraticaUsernameDestinatarioNavigation { get; set; }
        public virtual ICollection<RichiestaDocumentoPratica> RichiestaDocumentoPraticaUsernameRichiedenteNavigation { get; set; }
        public virtual ICollection<UtenteAssociazione> UtenteAssociazione { get; set; }
    }
}
