﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace InformaTOSicuro.Model
{
    public partial class InformaTOSicuroContext : DbContext
    {
        public InformaTOSicuroContext()
        {
        }

        public InformaTOSicuroContext(DbContextOptions<InformaTOSicuroContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccessToken> AccessToken { get; set; }
        public virtual DbSet<Associazione> Associazione { get; set; }
        public virtual DbSet<CanaleComunicazione> CanaleComunicazione { get; set; }
        public virtual DbSet<Comune> Comune { get; set; }
        public virtual DbSet<Comunicazione> Comunicazione { get; set; }
        public virtual DbSet<ContattoPratica> ContattoPratica { get; set; }
        public virtual DbSet<DocumentoPratica> DocumentoPratica { get; set; }
        public virtual DbSet<ErrorLog> ErrorLog { get; set; }
        public virtual DbSet<EsitoStatoPratica> EsitoStatoPratica { get; set; }
        public virtual DbSet<EventoPratica> EventoPratica { get; set; }
        public virtual DbSet<ListaAssociazione> ListaAssociazione { get; set; }
        public virtual DbSet<ListaComunicazione> ListaComunicazione { get; set; }
        public virtual DbSet<ListaContattoPratica> ListaContattoPratica { get; set; }
        public virtual DbSet<ListaDocumentoPratica> ListaDocumentoPratica { get; set; }
        public virtual DbSet<ListaEventoPratica> ListaEventoPratica { get; set; }
        public virtual DbSet<ListaMessaggioPratica> ListaMessaggioPratica { get; set; }
        public virtual DbSet<ListaModelloPratica> ListaModelloPratica { get; set; }
        public virtual DbSet<ListaModelloPraticaSpecializzazioneProfessionale> ListaModelloPraticaSpecializzazioneProfessionale { get; set; }
        public virtual DbSet<ListaModelloStatoPraticaTipoDocumento> ListaModelloStatoPraticaTipoDocumento { get; set; }
        public virtual DbSet<ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale> ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale { get; set; }
        public virtual DbSet<ListaModelloStatoPraticaTipoDocumentoTipoDestinatario> ListaModelloStatoPraticaTipoDocumentoTipoDestinatario { get; set; }
        public virtual DbSet<ListaPartecipanteRiunionePratica> ListaPartecipanteRiunionePratica { get; set; }
        public virtual DbSet<ListaPratica> ListaPratica { get; set; }
        public virtual DbSet<ListaRichiestaDocumentoPratica> ListaRichiestaDocumentoPratica { get; set; }
        public virtual DbSet<ListaRiunionePratica> ListaRiunionePratica { get; set; }
        public virtual DbSet<ListaUtente> ListaUtente { get; set; }
        public virtual DbSet<ListaUtenteAssociazione> ListaUtenteAssociazione { get; set; }
        public virtual DbSet<ListaUtentePratica> ListaUtentePratica { get; set; }
        public virtual DbSet<ListaUtenteSpecializzazioneProfessionale> ListaUtenteSpecializzazioneProfessionale { get; set; }
        public virtual DbSet<MessaggioPratica> MessaggioPratica { get; set; }
        public virtual DbSet<ModelloComunicazione> ModelloComunicazione { get; set; }
        public virtual DbSet<ModelloPratica> ModelloPratica { get; set; }
        public virtual DbSet<ModelloPraticaSpecializzazioneProfessionale> ModelloPraticaSpecializzazioneProfessionale { get; set; }
        public virtual DbSet<ModelloStatoPraticaTipoDocumento> ModelloStatoPraticaTipoDocumento { get; set; }
        public virtual DbSet<ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale> ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale { get; set; }
        public virtual DbSet<ModelloStatoPraticaTipoDocumentoTipoDestinatario> ModelloStatoPraticaTipoDocumentoTipoDestinatario { get; set; }
        public virtual DbSet<PartecipanteRiunionePratica> PartecipanteRiunionePratica { get; set; }
        public virtual DbSet<Pratica> Pratica { get; set; }
        public virtual DbSet<Provincia> Provincia { get; set; }
        public virtual DbSet<RichiestaDocumentoPratica> RichiestaDocumentoPratica { get; set; }
        public virtual DbSet<RiunionePratica> RiunionePratica { get; set; }
        public virtual DbSet<RuoloUtente> RuoloUtente { get; set; }
        public virtual DbSet<SpecializzazioneProfessionale> SpecializzazioneProfessionale { get; set; }
        public virtual DbSet<StatoPratica> StatoPratica { get; set; }
        public virtual DbSet<StatoUtente> StatoUtente { get; set; }
        public virtual DbSet<TipoDestinatario> TipoDestinatario { get; set; }
        public virtual DbSet<TipoDocumento> TipoDocumento { get; set; }
        public virtual DbSet<TipoEventoPratica> TipoEventoPratica { get; set; }
        public virtual DbSet<Utente> Utente { get; set; }
        public virtual DbSet<UtenteAssociazione> UtenteAssociazione { get; set; }
        public virtual DbSet<UtentePratica> UtentePratica { get; set; }
        public virtual DbSet<UtenteSpecializzazioneProfessionale> UtenteSpecializzazioneProfessionale { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=192.168.100.245;Database=InformaTOSicuro;Initial Catalog=InformaTOSicuro;Persist Security Info=False;User ID=InformaTOSicuro_API;Password=jC#BNp%kGcac");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccessToken>(entity =>
            {
                entity.Property(e => e.AccessToken1)
                    .HasColumnName("AccessToken")
                    .HasMaxLength(256);

                entity.Property(e => e.DataCreazione).HasColumnType("datetime");

                entity.Property(e => e.DataRefresh).HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.AccessToken)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccessToken_Utente");
            });

            modelBuilder.Entity<Associazione>(entity =>
            {
                entity.Property(e => e.Cap)
                    .HasColumnName("CAP")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodComune)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodiceFiscale)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("EMail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Indirizzo)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PartitaIva)
                    .HasColumnName("PartitaIVA")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.RagioneSociale)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CodComuneNavigation)
                    .WithMany(p => p.Associazione)
                    .HasForeignKey(d => d.CodComune)
                    .HasConstraintName("FK_Associazione_Comune");
            });

            modelBuilder.Entity<CanaleComunicazione>(entity =>
            {
                entity.HasKey(e => e.Cod);

                entity.Property(e => e.Cod)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Comune>(entity =>
            {
                entity.HasKey(e => e.Cod);

                entity.Property(e => e.Cod)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodCatastale)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Comune1)
                    .HasColumnName("Comune")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiglaProvincia)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.HasOne(d => d.SiglaProvinciaNavigation)
                    .WithMany(p => p.Comune)
                    .HasForeignKey(d => d.SiglaProvincia)
                    .HasConstraintName("FK_Comune_Provincia");
            });

            modelBuilder.Entity<Comunicazione>(entity =>
            {
                entity.Property(e => e.CodCanale)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodModello)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DataInvio).HasColumnType("datetime");

                entity.Property(e => e.DataPrevistoInvio).HasColumnType("datetime");

                entity.Property(e => e.Uid).HasColumnName("UID");

                entity.Property(e => e.UsernameDestinatario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CodCanaleNavigation)
                    .WithMany(p => p.Comunicazione)
                    .HasForeignKey(d => d.CodCanale)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comunicazione_CanaleComunicazione");

                entity.HasOne(d => d.CodModelloNavigation)
                    .WithMany(p => p.Comunicazione)
                    .HasForeignKey(d => d.CodModello)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comunicazione_ModelloComunicazione");

                entity.HasOne(d => d.UsernameDestinatarioNavigation)
                    .WithMany(p => p.Comunicazione)
                    .HasForeignKey(d => d.UsernameDestinatario)
                    .HasConstraintName("FK_Comunicazione_Utente");
            });

            modelBuilder.Entity<ContattoPratica>(entity =>
            {
                entity.HasKey(e => new { e.IdPratica, e.NumContatto });

                entity.Property(e => e.CodSpecializzazioneProfessionale)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Cognome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataUsername).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("EMail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdPraticaNavigation)
                    .WithMany(p => p.ContattoPratica)
                    .HasForeignKey(d => d.IdPratica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContattoPratica_Pratica");
            });

            modelBuilder.Entity<DocumentoPratica>(entity =>
            {
                entity.HasKey(e => new { e.IdPratica, e.NumDocumento });

                entity.Property(e => e.CodTipoDocumento)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DataCancellazione).HasColumnType("datetime");

                entity.Property(e => e.DataInserimento).HasColumnType("datetime");

                entity.Property(e => e.NomeFile)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.PercorsoFile).IsUnicode(false);

                entity.Property(e => e.TipoFile)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameInserimento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdPraticaNavigation)
                    .WithMany(p => p.DocumentoPratica)
                    .HasForeignKey(d => d.IdPratica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocumentoPratica_Pratica");

                entity.HasOne(d => d.UsernameInserimentoNavigation)
                    .WithMany(p => p.DocumentoPratica)
                    .HasForeignKey(d => d.UsernameInserimento)
                    .HasConstraintName("FK_DocumentoPratica_Utente");

                entity.HasOne(d => d.RichiestaDocumentoPratica)
                    .WithMany(p => p.DocumentoPratica)
                    .HasForeignKey(d => new { d.IdPratica, d.NumRichiesta })
                    .HasConstraintName("FK_DocumentoPratica_RichiestaDocumentoPratica");
            });

            modelBuilder.Entity<ErrorLog>(entity =>
            {
                entity.Property(e => e.Descrizione).IsUnicode(false);
            });

            modelBuilder.Entity<EsitoStatoPratica>(entity =>
            {
                entity.HasKey(e => new { e.CodStato, e.CodEsito });

                entity.Property(e => e.CodStato)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodEsito)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodStatoSuccessivo)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TestoConferma).IsUnicode(false);

                entity.HasOne(d => d.CodStatoNavigation)
                    .WithMany(p => p.EsitoStatoPraticaCodStatoNavigation)
                    .HasForeignKey(d => d.CodStato)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EsitoStatoPratica_StatoPratica");

                entity.HasOne(d => d.CodStatoSuccessivoNavigation)
                    .WithMany(p => p.EsitoStatoPraticaCodStatoSuccessivoNavigation)
                    .HasForeignKey(d => d.CodStatoSuccessivo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EsitoStatoPratica_StatoPraticaSuccessivo");
            });

            modelBuilder.Entity<EventoPratica>(entity =>
            {
                entity.HasKey(e => new { e.IdPratica, e.NumEvento });

                entity.Property(e => e.CodTipoEvento)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DataEvento).HasColumnType("datetime");

                entity.Property(e => e.Descrizione)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameEvento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CodTipoEventoNavigation)
                    .WithMany(p => p.EventoPratica)
                    .HasForeignKey(d => d.CodTipoEvento)
                    .HasConstraintName("FK_EventoPratica_TipoEventoPratica");

                entity.HasOne(d => d.IdPraticaNavigation)
                    .WithMany(p => p.EventoPratica)
                    .HasForeignKey(d => d.IdPratica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EventoPratica_Pratica");

                entity.HasOne(d => d.DocumentoPratica)
                    .WithMany(p => p.EventoPratica)
                    .HasForeignKey(d => new { d.IdPratica, d.NumDocumento })
                    .HasConstraintName("FK_EventoPratica_DocumentoPratica");
            });

            modelBuilder.Entity<ListaAssociazione>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaAssociazione");

                entity.Property(e => e.Cap)
                    .HasColumnName("CAP")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodComune)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodiceFiscale)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Comune)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("EMail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Indirizzo)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PartitaIva)
                    .HasColumnName("PartitaIVA")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.RagioneSociale)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiglaProvincia)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaComunicazione>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaComunicazione");

                entity.Property(e => e.Canale)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodCanale)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodModello)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodRuolo)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CognomeContatto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CognomeDestinatario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataInvio).HasColumnType("datetime");

                entity.Property(e => e.DataPrevistoInvio).HasColumnType("datetime");

                entity.Property(e => e.DataRiunione).HasColumnType("datetime");

                entity.Property(e => e.Documento)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailContatto)
                    .HasColumnName("EMailContatto")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmailDestinatario)
                    .HasColumnName("EMailDestinatario")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Modello)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeContatto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeDestinatario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Richiesta)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Riunione)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TitoloPratica)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Uid).HasColumnName("UID");

                entity.Property(e => e.UsernameDestinatario)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaContattoPratica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaContattoPratica");

                entity.Property(e => e.CodSpecializzazioneProfessionale)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Cognome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("EMail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SpecializzazioneProfessionale)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Titolo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameRichiedente)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaDocumentoPratica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaDocumentoPratica");

                entity.Property(e => e.CodTipoDocumento)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CognomeUtenteInserimento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataInserimento).HasColumnType("datetime");

                entity.Property(e => e.DescrizioneRichiesta)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NomeFile)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NomeUtenteInserimento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.PercorsoFile).IsUnicode(false);

                entity.Property(e => e.TipoDocumento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoFile)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Titolo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameInserimento)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaEventoPratica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaEventoPratica");

                entity.Property(e => e.CodTipoEvento)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CognomeUtenteEvento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataEvento).HasColumnType("datetime");

                entity.Property(e => e.Descrizione)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NomeUtenteEvento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pratica)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoEvento)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameEvento)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaMessaggioPratica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaMessaggioPratica");

                entity.Property(e => e.CognomeUtente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataInserimento).HasColumnType("datetime");

                entity.Property(e => e.Messaggio).IsUnicode(false);

                entity.Property(e => e.NomeUtente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaModelloPratica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaModelloPratica");

                entity.Property(e => e.Associazione)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CognomeUtenteCreazione)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataCreazione).HasColumnType("datetime");

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.NomeUtenteCreazione)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Titolo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameCreazione)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaModelloPraticaSpecializzazioneProfessionale>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaModelloPraticaSpecializzazioneProfessionale");

                entity.Property(e => e.CodSpecializzazioneProfessionale)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ModelloPratica)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SpecializzazioneProfessionale)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaModelloStatoPraticaTipoDocumento>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaModelloStatoPraticaTipoDocumento");

                entity.Property(e => e.CodStatoPratica)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodTipoDocumento)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ModelloPratica)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatoPratica)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoDocumento)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale");

                entity.Property(e => e.CodSpecializzazioneProfessionale)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodStatoPratica)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodTipoDocumento)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModelloPratica)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SpecializzazioneProfessionale)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoDocumento)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaModelloStatoPraticaTipoDocumentoTipoDestinatario>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaModelloStatoPraticaTipoDocumentoTipoDestinatario");

                entity.Property(e => e.CodStatoPratica)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodTipoDestinatario)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodTipoDocumento)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModelloPratica)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoDestinatario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoDocumento)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaPartecipanteRiunionePratica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaPartecipanteRiunionePratica");

                entity.Property(e => e.CognomePartecipante)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataConferma).HasColumnType("datetime");

                entity.Property(e => e.DataInserimento).HasColumnType("datetime");

                entity.Property(e => e.DataRiunione).HasColumnType("datetime");

                entity.Property(e => e.NomePartecipante)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.OggettoRiunione)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UsernamePartecipante)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaPratica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaPratica");

                entity.Property(e => e.Associazione)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CodEsitoStatoPraticaPrecedente)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodStatoPratica)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodStatoPraticaPrecedente)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CognomeResponsabile)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CognomeRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CognomeSegretaria)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ComuneResidenzaRichiedente)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DataRichiesta).HasColumnType("datetime");

                entity.Property(e => e.Descrizione).IsUnicode(false);

                entity.Property(e => e.EmailRichiedente)
                    .HasColumnName("EMailRichiedente")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EsitoStatoPraticaPrecedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModelloPratica)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeResponsabile)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeSegretaria)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Richiedente)
                    .IsRequired()
                    .HasMaxLength(101)
                    .IsUnicode(false);

                entity.Property(e => e.SiglaProvinciaResidenzaRichiedente)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.StatoPratica)
                    .IsRequired()
                    .HasMaxLength(103)
                    .IsUnicode(false);

                entity.Property(e => e.TelefonoRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Titolo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameResponsabile)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameRichiedente)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameSegretaria)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaRichiestaDocumentoPratica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaRichiestaDocumentoPratica");

                entity.Property(e => e.CodTipoDocumento)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CognomeDestinatario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CognomeRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataRichiesta).HasColumnType("datetime");

                entity.Property(e => e.Descrizione)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NomeDestinatario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoDocumento)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameDestinatario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaRiunionePratica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaRiunionePratica");

                entity.Property(e => e.CognomeRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataCancellazione).HasColumnType("datetime");

                entity.Property(e => e.DataRichiestaRiunione).HasColumnType("datetime");

                entity.Property(e => e.DataRiunione).HasColumnType("datetime");

                entity.Property(e => e.Descrizione).IsUnicode(false);

                entity.Property(e => e.Luogo)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NomeRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Oggetto)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Titolo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaUtente>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaUtente");

                entity.Property(e => e.Capresidenza)
                    .HasColumnName("CAPResidenza")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CivicoResidenza)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodComuneNascita)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodComuneResidenza)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodRuolo)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodStato)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodiceFiscale)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Cognome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ComuneNascita)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ComuneResidenza)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DataNascita).HasColumnType("date");

                entity.Property(e => e.Email)
                    .HasColumnName("EMail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IndirizzoResidenza)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PartitaIva)
                    .HasColumnName("PartitaIVA")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.RuoloUtente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiglaProvinciaNascita)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.SiglaprovinciaResidenza)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.StatoUtente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UrlCv)
                    .HasColumnName("UrlCV")
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaUtenteAssociazione>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaUtenteAssociazione");

                entity.Property(e => e.Associazione)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CognomeProfessionista)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataFineRapporto).HasColumnType("date");

                entity.Property(e => e.DataInizioRapporto).HasColumnType("date");

                entity.Property(e => e.NomeProfessionista)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Urlcv)
                    .HasColumnName("URLCV")
                    .IsUnicode(false);

                entity.Property(e => e.UsernameProfessionista)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaUtentePratica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaUtentePratica");

                entity.Property(e => e.CodSpecializzazioneProfessionale)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CognomeUtente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeUtente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pratica)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SpecializzazioneProfessionale)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ListaUtenteSpecializzazioneProfessionale>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ListaUtenteSpecializzazioneProfessionale");

                entity.Property(e => e.CodSpecializzazioneProfessionale)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CognomeUtente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NomeUtente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SpecializzazioneProfessionale)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MessaggioPratica>(entity =>
            {
                entity.Property(e => e.DataCancellazione).HasColumnType("datetime");

                entity.Property(e => e.DataInserimento).HasColumnType("datetime");

                entity.Property(e => e.Messaggio).IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdPraticaNavigation)
                    .WithMany(p => p.MessaggioPratica)
                    .HasForeignKey(d => d.IdPratica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MessaggioPratica_Pratica");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.MessaggioPratica)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MessaggioPratica_Utente");
            });

            modelBuilder.Entity<ModelloComunicazione>(entity =>
            {
                entity.HasKey(e => e.Cod);

                entity.Property(e => e.Cod)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ModelloPratica>(entity =>
            {
                entity.Property(e => e.DataCancellazione).HasColumnType("datetime");

                entity.Property(e => e.DataCreazione).HasColumnType("datetime");

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Titolo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameCreazione)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAssociazioneNavigation)
                    .WithMany(p => p.ModelloPratica)
                    .HasForeignKey(d => d.IdAssociazione)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ModelloPratica_Associazione");
            });

            modelBuilder.Entity<ModelloPraticaSpecializzazioneProfessionale>(entity =>
            {
                entity.HasKey(e => new { e.IdModelloPratica, e.CodSpecializzazioneProfessionale });

                entity.Property(e => e.CodSpecializzazioneProfessionale)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Quantita).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<ModelloStatoPraticaTipoDocumento>(entity =>
            {
                entity.HasKey(e => new { e.IdModelloPratica, e.CodStatoPratica, e.CodTipoDocumento });

                entity.Property(e => e.CodStatoPratica)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodTipoDocumento)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.CodStatoPraticaNavigation)
                    .WithMany(p => p.ModelloStatoPraticaTipoDocumento)
                    .HasForeignKey(d => d.CodStatoPratica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ModelloStatoPraticaTipoDocumento_StatoPratica");

                entity.HasOne(d => d.CodTipoDocumentoNavigation)
                    .WithMany(p => p.ModelloStatoPraticaTipoDocumento)
                    .HasForeignKey(d => d.CodTipoDocumento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ModelloStatoPraticaTipoDocumento_TipoDocumento");

                entity.HasOne(d => d.IdModelloPraticaNavigation)
                    .WithMany(p => p.ModelloStatoPraticaTipoDocumento)
                    .HasForeignKey(d => d.IdModelloPratica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ModelloStatoPraticaTipoDocumento_ModelloPratica");
            });

            modelBuilder.Entity<ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale>(entity =>
            {
                entity.HasKey(e => new { e.IdModelloPratica, e.CodStatoPratica, e.CodTipoDocumento, e.CodSpecializzazioneProfessionale });

                entity.Property(e => e.CodStatoPratica)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodTipoDocumento)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodSpecializzazioneProfessionale)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.ModelloPraticaSpecializzazioneProfessionale)
                    .WithMany(p => p.ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale)
                    .HasForeignKey(d => new { d.IdModelloPratica, d.CodSpecializzazioneProfessionale })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale_ModelloPraticaSpecializzazioneProfessionale");

                entity.HasOne(d => d.ModelloStatoPraticaTipoDocumento)
                    .WithMany(p => p.ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale)
                    .HasForeignKey(d => new { d.IdModelloPratica, d.CodStatoPratica, d.CodTipoDocumento })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale_ModelloStatoPraticaTipoDocumento");
            });

            modelBuilder.Entity<ModelloStatoPraticaTipoDocumentoTipoDestinatario>(entity =>
            {
                entity.HasKey(e => new { e.IdModelloPratica, e.CodStatoPratica, e.CodTipoDocumento, e.CodTipoDestinatario });

                entity.Property(e => e.CodStatoPratica)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodTipoDocumento)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodTipoDestinatario)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.CodTipoDestinatarioNavigation)
                    .WithMany(p => p.ModelloStatoPraticaTipoDocumentoTipoDestinatario)
                    .HasForeignKey(d => d.CodTipoDestinatario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ModelloStatoPraticaTipoDocumentoTipoDestinatario_TipoDestinatario");

                entity.HasOne(d => d.ModelloStatoPraticaTipoDocumento)
                    .WithMany(p => p.ModelloStatoPraticaTipoDocumentoTipoDestinatario)
                    .HasForeignKey(d => new { d.IdModelloPratica, d.CodStatoPratica, d.CodTipoDocumento })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ModelloStatoPraticaTipoDocumentoTipoDestinatario_ModelloStatoPraticaTipoDocumento");
            });

            modelBuilder.Entity<PartecipanteRiunionePratica>(entity =>
            {
                entity.HasKey(e => new { e.IdRiunionePratica, e.UsernamePartecipante });

                entity.Property(e => e.UsernamePartecipante)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataCancellazione).HasColumnType("datetime");

                entity.Property(e => e.DataConferma).HasColumnType("datetime");

                entity.Property(e => e.DataInserimento).HasColumnType("datetime");

                entity.Property(e => e.Note).IsUnicode(false);

                entity.HasOne(d => d.UsernamePartecipanteNavigation)
                    .WithMany(p => p.PartecipanteRiunionePratica)
                    .HasForeignKey(d => d.UsernamePartecipante)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PartecipanteRiunionePratica_Utente");
            });

            modelBuilder.Entity<Pratica>(entity =>
            {
                entity.Property(e => e.CodEsitoStatoPraticaPrecedente)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodStatoPratica)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodStatoPraticaPrecedente)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DataCancellazione).HasColumnType("date");

                entity.Property(e => e.DataRichiesta).HasColumnType("datetime");

                entity.Property(e => e.Descrizione).IsUnicode(false);

                entity.Property(e => e.Titolo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameResponsabile)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameRichiedente)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameSegretaria)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CodStatoPraticaNavigation)
                    .WithMany(p => p.Pratica)
                    .HasForeignKey(d => d.CodStatoPratica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Pratica_StatoPratica");

                entity.HasOne(d => d.IdAssociazioneNavigation)
                    .WithMany(p => p.Pratica)
                    .HasForeignKey(d => d.IdAssociazione)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Pratica_Associazione");

                entity.HasOne(d => d.IdModelloNavigation)
                    .WithMany(p => p.Pratica)
                    .HasForeignKey(d => d.IdModello)
                    .HasConstraintName("FK_Pratica_ModelloPratica");

                entity.HasOne(d => d.UsernameResponsabileNavigation)
                    .WithMany(p => p.PraticaUsernameResponsabileNavigation)
                    .HasForeignKey(d => d.UsernameResponsabile)
                    .HasConstraintName("FK_Pratica_UtenteResponsabile");

                entity.HasOne(d => d.UsernameRichiedenteNavigation)
                    .WithMany(p => p.PraticaUsernameRichiedenteNavigation)
                    .HasForeignKey(d => d.UsernameRichiedente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Pratica_UtenteRichiedente");
            });

            modelBuilder.Entity<Provincia>(entity =>
            {
                entity.HasKey(e => e.Sigla);

                entity.Property(e => e.Sigla)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Provincia1)
                    .HasColumnName("Provincia")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Regione)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RichiestaDocumentoPratica>(entity =>
            {
                entity.HasKey(e => new { e.IdPratica, e.NumRichiesta });

                entity.Property(e => e.CodTipoDocumento)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DataCancellazione).HasColumnType("datetime");

                entity.Property(e => e.DataRichiesta).HasColumnType("datetime");

                entity.Property(e => e.Descrizione)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameDestinatario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CodTipoDocumentoNavigation)
                    .WithMany(p => p.RichiestaDocumentoPratica)
                    .HasForeignKey(d => d.CodTipoDocumento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RichiestaDocumentoPratica_TipoDocumento");

                entity.HasOne(d => d.IdPraticaNavigation)
                    .WithMany(p => p.RichiestaDocumentoPratica)
                    .HasForeignKey(d => d.IdPratica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RichiestaDocumentoPratica_Pratica");

                entity.HasOne(d => d.UsernameDestinatarioNavigation)
                    .WithMany(p => p.RichiestaDocumentoPraticaUsernameDestinatarioNavigation)
                    .HasForeignKey(d => d.UsernameDestinatario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RichiestaDocumentoPratica_UtenteDestinatario");

                entity.HasOne(d => d.UsernameRichiedenteNavigation)
                    .WithMany(p => p.RichiestaDocumentoPraticaUsernameRichiedenteNavigation)
                    .HasForeignKey(d => d.UsernameRichiedente)
                    .HasConstraintName("FK_RichiestaDocumentoPratica_UtenteRichiedente");
            });

            modelBuilder.Entity<RiunionePratica>(entity =>
            {
                entity.Property(e => e.DataCancellazione).HasColumnType("datetime");

                entity.Property(e => e.DataRichiesta).HasColumnType("datetime");

                entity.Property(e => e.DataRiunione).HasColumnType("datetime");

                entity.Property(e => e.Descrizione).IsUnicode(false);

                entity.Property(e => e.Luogo)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Oggetto)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UsernameRichiedente)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RuoloUtente>(entity =>
            {
                entity.HasKey(e => e.Cod);

                entity.Property(e => e.Cod)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SpecializzazioneProfessionale>(entity =>
            {
                entity.HasKey(e => e.Cod);

                entity.Property(e => e.Cod)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DataCancellazione).HasColumnType("datetime");

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<StatoPratica>(entity =>
            {
                entity.HasKey(e => e.Cod);

                entity.Property(e => e.Cod)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodTipoEvento)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CodTipoEventoNavigation)
                    .WithMany(p => p.StatoPratica)
                    .HasForeignKey(d => d.CodTipoEvento)
                    .HasConstraintName("FK_StatoPratica_TipoEventoPratica");
            });

            modelBuilder.Entity<StatoUtente>(entity =>
            {
                entity.HasKey(e => e.Cod);

                entity.Property(e => e.Cod)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TipoDestinatario>(entity =>
            {
                entity.HasKey(e => e.Cod);

                entity.Property(e => e.Cod)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TipoDocumento>(entity =>
            {
                entity.HasKey(e => e.Cod);

                entity.Property(e => e.Cod)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DataCancellazione).HasColumnType("datetime");

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TipoEventoPratica>(entity =>
            {
                entity.HasKey(e => e.Cod);

                entity.Property(e => e.Cod)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Utente>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Capresidenza)
                    .HasColumnName("CAPResidenza")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CivicoResidenza)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodComuneNascita)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodComuneResidenza)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodRuolo)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodStato)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodiceFiscale)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Cognome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataNascita).HasColumnType("date");

                entity.Property(e => e.Email)
                    .HasColumnName("EMail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IndirizzoResidenza)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PartitaIva)
                    .HasColumnName("PartitaIVA")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Password).HasMaxLength(8000);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UrlCv)
                    .HasColumnName("UrlCV")
                    .IsUnicode(false);

                entity.HasOne(d => d.CodComuneNascitaNavigation)
                    .WithMany(p => p.UtenteCodComuneNascitaNavigation)
                    .HasForeignKey(d => d.CodComuneNascita)
                    .HasConstraintName("FK_Utente_ComuneNascita");

                entity.HasOne(d => d.CodComuneResidenzaNavigation)
                    .WithMany(p => p.UtenteCodComuneResidenzaNavigation)
                    .HasForeignKey(d => d.CodComuneResidenza)
                    .HasConstraintName("FK_Utente_ComuneResidenza");

                entity.HasOne(d => d.CodRuoloNavigation)
                    .WithMany(p => p.Utente)
                    .HasForeignKey(d => d.CodRuolo)
                    .HasConstraintName("FK_Utente_RuoloUtente");

                entity.HasOne(d => d.CodStatoNavigation)
                    .WithMany(p => p.Utente)
                    .HasForeignKey(d => d.CodStato)
                    .HasConstraintName("FK_Utente_StatoUtente");
            });

            modelBuilder.Entity<UtenteAssociazione>(entity =>
            {
                entity.HasKey(e => new { e.Username, e.IdAssociazione });

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataFineRapporto).HasColumnType("date");

                entity.Property(e => e.DataInizioRapporto).HasColumnType("date");

                entity.Property(e => e.Urlcv)
                    .HasColumnName("URLCV")
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAssociazioneNavigation)
                    .WithMany(p => p.UtenteAssociazione)
                    .HasForeignKey(d => d.IdAssociazione)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UtenteAssociazione_Associazione");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.UtenteAssociazione)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UtenteAssociazione_Utente");
            });

            modelBuilder.Entity<UtentePratica>(entity =>
            {
                entity.HasKey(e => new { e.IdPratica, e.Username });

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodSpecializzazioneProfessionale)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<UtenteSpecializzazioneProfessionale>(entity =>
            {
                entity.HasKey(e => new { e.Username, e.CodSpecializzazioneProfessionale });

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodSpecializzazioneProfessionale)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
