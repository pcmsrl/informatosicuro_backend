﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaPraticaMessaggio
    {
        public long Id { get; set; }
        public long IdPratica { get; set; }
        public string Username { get; set; }
        public string NomeUtente { get; set; }
        public string CognomeUtente { get; set; }
        public string Messaggio { get; set; }
        public DateTime DataInserimento { get; set; }
    }
}
