﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class AccessToken
    {
        public long Id { get; set; }
        public byte[] AccessToken1 { get; set; }
        public DateTime DataCreazione { get; set; }
        public string Username { get; set; }
        public DateTime? DataRefresh { get; set; }

        public virtual Utente UsernameNavigation { get; set; }
    }
}
