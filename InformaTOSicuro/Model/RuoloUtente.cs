﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class RuoloUtente
    {
        public RuoloUtente()
        {
            Utente = new HashSet<Utente>();
        }

        public string Cod { get; set; }
        public string Descrizione { get; set; }

        public virtual ICollection<Utente> Utente { get; set; }
    }
}
