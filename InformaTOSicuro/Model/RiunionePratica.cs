﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class RiunionePratica
    {
        public long Id { get; set; }
        public long IdPratica { get; set; }
        public DateTime DataRichiesta { get; set; }
        public DateTime DataRiunione { get; set; }
        public string Oggetto { get; set; }
        public string Descrizione { get; set; }
        public DateTime? DataCancellazione { get; set; }
        public string Luogo { get; set; }
        public string UsernameRichiedente { get; set; }
    }
}
