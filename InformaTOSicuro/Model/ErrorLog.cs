﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ErrorLog
    {
        public long Id { get; set; }
        public string Descrizione { get; set; }
    }
}
