﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaModelloPratica
    {
        public int Id { get; set; }
        public int IdAssociazione { get; set; }
        public string Associazione { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public string UsernameCreazione { get; set; }
        public string CognomeUtenteCreazione { get; set; }
        public string NomeUtenteCreazione { get; set; }
        public DateTime DataCreazione { get; set; }
    }
}
