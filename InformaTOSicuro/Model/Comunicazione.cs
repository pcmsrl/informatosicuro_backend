﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class Comunicazione
    {
        public long Id { get; set; }
        public string CodCanale { get; set; }
        public string CodModello { get; set; }
        public string UsernameDestinatario { get; set; }
        public DateTime? DataPrevistoInvio { get; set; }
        public DateTime? DataInvio { get; set; }
        public long? IdPratica { get; set; }
        public Guid? Uid { get; set; }
        public short? NumRichiesta { get; set; }
        public long? IdRiunione { get; set; }
        public short? NumDocumento { get; set; }
        public short? NumContatto { get; set; }

        public virtual CanaleComunicazione CodCanaleNavigation { get; set; }
        public virtual ModelloComunicazione CodModelloNavigation { get; set; }
        public virtual Utente UsernameDestinatarioNavigation { get; set; }
    }
}
