﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class PartecipanteRiunionePratica
    {
        public long IdRiunionePratica { get; set; }
        public string UsernamePartecipante { get; set; }
        public DateTime DataInserimento { get; set; }
        public DateTime? DataConferma { get; set; }
        public DateTime? DataCancellazione { get; set; }
        public string Note { get; set; }

        public virtual Utente UsernamePartecipanteNavigation { get; set; }
    }
}
