﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaPartecipanteRiunionePratica
    {
        public long IdRiunionePratica { get; set; }
        public long IdPratica { get; set; }
        public DateTime DataRiunione { get; set; }
        public string OggettoRiunione { get; set; }
        public string UsernamePartecipante { get; set; }
        public string NomePartecipante { get; set; }
        public string CognomePartecipante { get; set; }
        public DateTime DataInserimento { get; set; }
        public DateTime? DataConferma { get; set; }
        public string Note { get; set; }
    }
}
