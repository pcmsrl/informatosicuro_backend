﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaRiunionePratica
    {
        public long Id { get; set; }
        public long IdPratica { get; set; }
        public string Titolo { get; set; }
        public DateTime DataRichiestaRiunione { get; set; }
        public DateTime DataRiunione { get; set; }
        public string Oggetto { get; set; }
        public string Descrizione { get; set; }
        public DateTime? DataCancellazione { get; set; }
        public string Luogo { get; set; }
        public string UsernameRichiedente { get; set; }
        public string NomeRichiedente { get; set; }
        public string CognomeRichiedente { get; set; }
    }
}
