﻿using System;
using System.Collections.Generic;

namespace InformaTOSicuro.Model
{
    public partial class ListaModelloStatoPraticaTipoDocumentoSpecializzazioneProfessionale
    {
        public int IdModelloPratica { get; set; }
        public int IdAssociazione { get; set; }
        public string ModelloPratica { get; set; }
        public string CodStatoPratica { get; set; }
        public string Descrizione { get; set; }
        public string CodTipoDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public string CodSpecializzazioneProfessionale { get; set; }
        public string SpecializzazioneProfessionale { get; set; }
    }
}
