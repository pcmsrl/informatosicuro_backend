﻿using InformaTOSicuro.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace InformaTOSicuro.Middleware
{
    public class Authentication
    {
        private readonly RequestDelegate _next;

        public Authentication(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, InformaTOSicuroContext dbContext)
        {

            string authHeader = httpContext.Request.Headers["Authorization"];
            ListaUtente listaUtente = null;
            byte[] refreshToken = null;

            if (authHeader != null)
            {
                
                var authHeaderValue = AuthenticationHeaderValue.Parse(authHeader);
                if (authHeaderValue.Scheme == "Bearer")
                {
                    byte[] token = Convert.FromBase64String(authHeaderValue.Parameter);

                    listaUtente =  Model.Utente.Select(dbContext, token,ref refreshToken);

                };
                if (authHeaderValue.Scheme.Equals(AuthenticationSchemes.Basic.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    var credentials = Encoding.UTF8
                                        .GetString(Convert.FromBase64String(authHeaderValue.Parameter ?? string.Empty))
                                        .Split(':', 2);
                    if (credentials.Length == 2)
                    {

                        listaUtente = await Utente.Select(dbContext, credentials[0], credentials[1]);

                    }
                };

            }

            if (listaUtente != null)
            {

                httpContext.User = new GenericPrincipal(new ClaimsIdentity(listaUtente.Username), null);
                if (refreshToken != null)
                { 
                    httpContext.Response.Headers.Add("refreshToken",Convert.ToBase64String(refreshToken));
                    httpContext.Response.Headers.Add("Access-Control-Expose-Headers", "refreshToken");
                }

            }

            await _next.Invoke(httpContext);

        }

            
    }
}
