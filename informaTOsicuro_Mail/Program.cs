﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Mail;
using InformaTOSicuro;
using InformaTOSicuro.Model;
using Microsoft.EntityFrameworkCore;

namespace informaTOsicuro_Mail
{
    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("Starting sending mail...");

            DbContextOptionsBuilder<InformaTOSicuroContext> optionsBuilder = new DbContextOptionsBuilder<InformaTOSicuroContext>();

            //optionsBuilder.UseSqlServer("Server=192.168.100.245;Database=InformaTOSicuro;Initial Catalog=InformaTOSicuro;Persist Security Info=False;User ID=InformaTOSicuro_API;Password=jC#BNp%kGcac");
            optionsBuilder.UseSqlServer("Server=85.234.143.96, 1987;Database=informaTOsicuro_test;User Id=InformaTOSicuro_API;Password=$t7S4U3@^r92k&qk;MultipleActiveResultSets=true");
            //optionsBuilder.UseSqlServer("Server=95.110.232.15, 50013;Database=informaTOsicuro;Initial Catalog=informaTOsicuro;Persist Security Info=False;User ID=informaTOsicuro_API;Password=Jwt4JJ+4");


            InformaTOSicuroContext context = new InformaTOSicuroContext(optionsBuilder.Options);

            var listaComunicazione = context.ListaComunicazione
                .Where(x => x.DataInvio == null)
                .OrderBy(x => x.DataPrevistoInvio).ToList();

            foreach (var comunicazione in listaComunicazione)
            {

                try
                { 

                    Mail.SendMail(comunicazione);
                    Console.WriteLine(string.Concat("Sending mail ",comunicazione.Id));

                    comunicazione.DataInvio = DateTime.Now;
                    comunicazione.SetDataInvio(context);

                }
                catch(Exception ex)
                {
                    Console.WriteLine(string.Concat(comunicazione.ToString()," ",ex.Message));
                }

            }



        }
    }
}
