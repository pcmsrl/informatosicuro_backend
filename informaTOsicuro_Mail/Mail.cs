﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using InformaTOSicuro.Model;

namespace informaTOsicuro_Mail
{
    class Mail
    {
        private static readonly string _path = "\\Template";

        public static void SendMail(ListaComunicazione listaComunicazione)
        {
            /*
            SmtpClient SmtpClient = new SmtpClient("smtp.office365.com", 587)
            {
                UseDefaultCredentials = false,
                EnableSsl = true
            };
            
            SmtpClient.Credentials = new NetworkCredential("enisuscsf@enisus.com", "3n1susC0");

            MailAddress from = new MailAddress("enisuscsf@enisus.com");
            */
            // test 
            
            SmtpClient SmtpClient = new SmtpClient("smtps.aruba.it", 587)
            {
                UseDefaultCredentials = false,
                EnableSsl = true
            };

            SmtpClient.Credentials = new NetworkCredential("pratiche@informatosicuro.it", "Olla2020");

            MailAddress from = new MailAddress("pratiche@informatosicuro.it");

            // prod
            /*
            SmtpClient SmtpClient = new SmtpClient("smtps.aruba.it", 587) //465)
            {
                UseDefaultCredentials = false,
                EnableSsl = true
            };

            SmtpClient.Credentials = new NetworkCredential("desk@informatosicuro.it", "D3sK2021!!");

            MailAddress from = new MailAddress("desk@informatosicuro.it");*/


            //MailAddress to = new MailAddress("tognin@pcmitaly.it");
            MailAddress to = new MailAddress(string.IsNullOrEmpty(listaComunicazione.EmailDestinatario) ? listaComunicazione.EmailContatto : listaComunicazione.EmailDestinatario);
            MailMessage message = new MailMessage(from, to);

            string template = string.Concat(System.IO.Directory.GetCurrentDirectory(),_path,"\\", listaComunicazione.CodModello,".html");

            StreamReader reader = new StreamReader(template);

            message.IsBodyHtml = true;
            message.Body = reader.ReadToEnd();

            message.Body = message.Body.Replace("{NomeDestinatario}", listaComunicazione.NomeDestinatario);
            message.Body = message.Body.Replace("{CognomeDestinatario}", listaComunicazione.CognomeDestinatario);
            message.Body = message.Body.Replace("{NomeContatto}", listaComunicazione.NomeContatto);
            message.Body = message.Body.Replace("{CognomeContatto}", listaComunicazione.CognomeContatto);
            message.Body = message.Body.Replace("{TitoloPratica}", listaComunicazione.TitoloPratica);
            message.Body = message.Body.Replace("{Documento}", listaComunicazione.Documento);
            message.Body = message.Body.Replace("{Riunione}", listaComunicazione.Riunione);
            message.Body = message.Body.Replace("{Richiesta}", listaComunicazione.Richiesta);
            message.Body = message.Body.Replace("{Username}", listaComunicazione.UsernameDestinatario);

            if (listaComunicazione.DataRiunione != null)
            {

                DateTime dataRiunione = listaComunicazione.DataRiunione.Value;
                message.Body = message.Body.Replace("{DataRiunione}", dataRiunione.ToString("dd/MM/yyyy hh:mm"));

            }

            message.Body = message.Body.Replace("{path}", "http://85.234.143.96:82");
            //message.Body = message.Body.Replace("{path}", "http://95.110.232.15:82");
            message.Body = message.Body.Replace("{uid}", (listaComunicazione.Uid == null) ? string.Empty : listaComunicazione.Uid.ToString() );

            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Subject = listaComunicazione.Modello;
            message.SubjectEncoding = System.Text.Encoding.UTF8;

            SmtpClient.Send(message);

        }

    }
}
